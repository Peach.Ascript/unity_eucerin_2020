﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AfterGame : MonoBehaviour
{
    [SerializeField] GameObject Cleanser, Moistur, Sun;
    [SerializeField] GameObject textTitle, textInfo;
    [SerializeField] GameObject ansCleanser, ansMoistur, ansSun;
    [SerializeField] GameObject imgCleanser, imgMoistur, imgSun;
    [SerializeField] Text textC, textM, textS, textM2, textSp, textS2;
    [SerializeField] GameObject canvasType1, canvasType2;
    [SerializeField] GameObject ansMoistur2, ansSpot2, ansSun2;

    private List<string> product = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(Confic.stateGame);
        if (Confic.skinType == 1 || Confic.skinType == 2 || Confic.skinType == 3 ||
            Confic.skinType == 4 || Confic.skinType == 6)
        {
            canvasType1.SetActive(true);
            canvasType2.SetActive(false);
            if(Confic.skinType == 1)
            {
                textTitle.GetComponent<Text>().text = "Atopic Dematitis".ToString();
                textInfo.GetComponent<Text>().text = "Dry and Atopic dermatitis regimen".ToString();
            }
            else if (Confic.skinType == 2)
            {
                textTitle.GetComponent<Text>().text = "Dry and Cracked skin".ToString();
                textInfo.GetComponent<Text>().text = "Dry and cracked regimen".ToString();
            }
            else if (Confic.skinType == 3)
            {
                textTitle.GetComponent<Text>().text = "Acne".ToString();
                textInfo.GetComponent<Text>().text = "ACNE-PRONE SKIN REGIMEN".ToString();
            }
            else if (Confic.skinType == 4)
            {
                textTitle.GetComponent<Text>().text = "Contact Dermatitis".ToString();
                textInfo.GetComponent<Text>().text = "Contact dermatitis regimen".ToString();
            }
            else if (Confic.skinType == 6)
            {
                textTitle.GetComponent<Text>().text = "AFTER LASER".ToString();
                textInfo.GetComponent<Text>().text = "AFTER LASER REGIMEN".ToString();
            }
        }
        if (Confic.skinType == 5)
        {
            canvasType1.SetActive(false);
            canvasType2.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Confic.stateGame == 1)
        {
            if(Confic.skinType == 1)
            {
                AnsNull();
                Answer();

                imgCleanser.GetComponent<RectTransform>().localPosition = new Vector3(-250, 150,0);
                imgMoistur.GetComponent<RectTransform>().localPosition = new Vector3(250, 150, 0);
                imgSun.SetActive(false);

                if (Confic.inSlotMoistur == "4")
                {
                    Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/1") as Texture2D;
                    Cleanser.GetComponent<RawImage>().SetNativeSize();
                    textC.text = "Ph5 perfume free".ToString();
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/4") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Aquaphor".ToString();
                }
                else if(Confic.inSlotMoistur == "6")
                {
                    Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/1") as Texture2D;
                    Cleanser.GetComponent<RawImage>().SetNativeSize();
                    textC.text = "Ph5 perfume free".ToString();
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/6") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Omega Plus".ToString();
                }
                else if (Confic.inSlotMoistur == "12")
                {
                    Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/1") as Texture2D;
                    Cleanser.GetComponent<RawImage>().SetNativeSize();
                    textC.text = "Ph5 perfume free".ToString();
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/12") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Omega lotion 250ml".ToString();
                }
                else if (Confic.inSlotMoistur == "13")
                {
                    Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/1") as Texture2D;
                    Cleanser.GetComponent<RawImage>().SetNativeSize();
                    textC.text = "Ph5 perfume free".ToString();
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/13") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Complete Repair 250ml".ToString();
                }
                else
                {
                    Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/1") as Texture2D;
                    Cleanser.GetComponent<RawImage>().SetNativeSize();
                    textC.text = "Ph5 perfume free".ToString();
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/4") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Aquaphor".ToString();
                }
            }
            else if(Confic.skinType == 2)
            {
                AnsNull();
                Answer();

                imgCleanser.GetComponent<RectTransform>().localPosition = new Vector3(-250, 150, 0);
                imgMoistur.GetComponent<RectTransform>().localPosition = new Vector3(250, 150, 0);
                imgSun.SetActive(false);

                if (Confic.inSlotCleaser == "1")
                {
                    Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/1") as Texture2D;
                    Cleanser.GetComponent<RawImage>().SetNativeSize();
                    textC.text = "Ph5 perfume free".ToString();
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/4") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Aquaphor".ToString();
                }
                else if (Confic.inSlotCleaser == "2")
                {
                    Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/2") as Texture2D;
                    Cleanser.GetComponent<RawImage>().SetNativeSize();
                    textC.text = "Baby".ToString();
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/4") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Aquaphor".ToString();
                }
                else
                {
                    Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/1") as Texture2D;
                    Cleanser.GetComponent<RawImage>().SetNativeSize();
                    textC.text = "Ph5 perfume free".ToString();
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/4") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Aquaphor".ToString();
                }
            }
            else if (Confic.skinType == 3)
            {
                AnsNull();
                Answer();

                Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/3") as Texture2D;
                Cleanser.GetComponent<RawImage>().SetNativeSize();
                textC.text = "Acne gel".ToString();
                Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/8") as Texture2D;
                Moistur.GetComponent<RawImage>().SetNativeSize();
                textM.text = "Active clear".ToString();
                Sun.GetComponent<RawImage>().texture = Resources.Load("page/page3/14") as Texture2D;
                Sun.GetComponent<RawImage>().SetNativeSize();
                textS.text = "Sun Dry Touch".ToString();
            }
            else if (Confic.skinType == 4)
            {
                AnsNull();
                Answer();

                imgCleanser.GetComponent<RectTransform>().localPosition = new Vector3(-250, 150, 0);
                imgMoistur.GetComponent<RectTransform>().localPosition = new Vector3(250, 150, 0);
                imgSun.SetActive(false);

                Cleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/1") as Texture2D;
                Cleanser.GetComponent<RawImage>().SetNativeSize();
                textC.text = "Ph5 perfume free".ToString();
                Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/7") as Texture2D;
                Moistur.GetComponent<RawImage>().SetNativeSize();
                textM.text = "Instant".ToString();
            }
            else if (Confic.skinType == 5)
            {
                AnsNull();
                Answer();
            }
            else if (Confic.skinType == 6)
            {
                AnsNull();
                Answer();

                imgCleanser.SetActive(false);
                imgMoistur.GetComponent<RectTransform>().localPosition = new Vector3(-250, 150, 0);
                imgSun.GetComponent<RectTransform>().localPosition = new Vector3(250, 150, 0);

                if (Confic.inSlotMoistur == "4")
                {
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/4") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Aquaphor".ToString();
                    Sun.GetComponent<RawImage>().texture = Resources.Load("page/page3/15") as Texture2D;
                    Sun.GetComponent<RawImage>().SetNativeSize();
                    textS.text = "Sun Fluid".ToString();
                }
                else if (Confic.inSlotMoistur == "7")
                {
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/7") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Instant".ToString();
                    Sun.GetComponent<RawImage>().texture = Resources.Load("page/page3/15") as Texture2D;
                    Sun.GetComponent<RawImage>().SetNativeSize();
                    textS.text = "Sun Fluid".ToString();
                }
                else
                {
                    Moistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/4") as Texture2D;
                    Moistur.GetComponent<RawImage>().SetNativeSize();
                    textM.text = "Aquaphor".ToString();
                    Sun.GetComponent<RawImage>().texture = Resources.Load("page/page3/15") as Texture2D;
                    Sun.GetComponent<RawImage>().SetNativeSize();
                    textS.text = "Sun Fluid".ToString();
                }
            }
        }
    }
    public void Answer()
    {
        ansCleanser.GetComponent<RawImage>().texture = Resources.Load("page/page3/" + Confic.inSlotCleaser) as Texture2D;
        ansCleanser.GetComponent<RawImage>().SetNativeSize();
        ansMoistur.GetComponent<RawImage>().texture = Resources.Load("page/page3/" + Confic.inSlotMoistur) as Texture2D;
        ansMoistur.GetComponent<RawImage>().SetNativeSize();
        ansSun.GetComponent<RawImage>().texture = Resources.Load("page/page3/" + Confic.inSlotSun) as Texture2D;
        ansSun.GetComponent<RawImage>().SetNativeSize();

        ansMoistur2.GetComponent<RawImage>().texture = Resources.Load("page/page3/" + Confic.inSlotMoistur) as Texture2D;
        ansMoistur2.GetComponent<RawImage>().SetNativeSize();
        ansSpot2.GetComponent<RawImage>().texture = Resources.Load("page/page3/" + Confic.inSlotSpot) as Texture2D;
        ansSpot2.GetComponent<RawImage>().SetNativeSize();
        ansSun2.GetComponent<RawImage>().texture = Resources.Load("page/page3/" + Confic.inSlotSun) as Texture2D;
        ansSun2.GetComponent<RawImage>().SetNativeSize();
    }
    public void AnsNull()
    {
        if (Confic.inSlotCleaser == null)
        {
            ansCleanser.SetActive(false);

            ansMoistur.GetComponent<RectTransform>().localPosition = new Vector3(-250, -312, 0);
            ansSun.GetComponent<RectTransform>().localPosition = new Vector3(250, -312, 0);
        }
        if (Confic.inSlotMoistur == null)
        {
            ansMoistur.SetActive(false);
            ansMoistur2.SetActive(false);

            ansCleanser.GetComponent<RectTransform>().localPosition = new Vector3(-250, -312, 0);
            ansSun.GetComponent<RectTransform>().localPosition = new Vector3(250, -312, 0);

            ansSpot2.GetComponent<RectTransform>().localPosition = new Vector3(-250, -312, 0);
            ansSun2.GetComponent<RectTransform>().localPosition = new Vector3(250, -312, 0);
        }
        if (Confic.inSlotSun == null)
        {
            ansSun.SetActive(false);
            ansSun2.SetActive(false);

            ansCleanser.GetComponent<RectTransform>().localPosition = new Vector3(-250, -312, 0);
            ansMoistur.GetComponent<RectTransform>().localPosition = new Vector3(250, -312, 0);

            ansMoistur2.GetComponent<RectTransform>().localPosition = new Vector3(-250, -312, 0);
            ansSpot2.GetComponent<RectTransform>().localPosition = new Vector3(250, -312, 0);
        }
        if (Confic.inSlotSpot == null)
        {
            ansSun2.SetActive(false);

            ansMoistur2.GetComponent<RectTransform>().localPosition = new Vector3(-250, -312, 0);
            ansSun2.GetComponent<RectTransform>().localPosition = new Vector3(250, -312, 0);
        }
        if (Confic.inSlotCleaser == null && Confic.inSlotMoistur == null)
        {
            ansSun.GetComponent<RectTransform>().localPosition = new Vector3(0, -312, 0);
        }
        if (Confic.inSlotMoistur == null && Confic.inSlotSun == null)
        {
            ansCleanser.GetComponent<RectTransform>().localPosition = new Vector3(0, -312, 0);
        }
        if (Confic.inSlotCleaser == null && Confic.inSlotSun == null)
        {
            ansMoistur.GetComponent<RectTransform>().localPosition = new Vector3(0, -312, 0);
        }
        if (Confic.inSlotSpot == null && Confic.inSlotSun == null)
        {
            ansMoistur2.GetComponent<RectTransform>().localPosition = new Vector3(0, -312, 0);
        }
        if (Confic.inSlotSpot == null && Confic.inSlotMoistur == null)
        {
            ansSun2.GetComponent<RectTransform>().localPosition = new Vector3(0, -312, 0);
        }
        if (Confic.inSlotSun == null && Confic.inSlotMoistur == null)
        {
            ansSpot2.GetComponent<RectTransform>().localPosition = new Vector3(0, -312, 0);
        }

    }
    public void Done()
    {
        StartCoroutine(ChangeScene("01Main"));
    }
    IEnumerator ChangeScene(string sceneName, float delay = .5f)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(sceneName);
    }
}
