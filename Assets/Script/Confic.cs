﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Confic : MonoBehaviour
{
    public static int stateGame = 0;
    //01Main
    public static int skinType = 0;
    //03Game
    public static bool submit = false;

    public static bool imgSlotC = true;
    public static bool imgSlotM = true;

    public static bool slotCleanser = false;
    public static string inSlotCleaser = null;

    public static bool slotMoistur = false;
    public static string inSlotMoistur = null;

    public static bool slotSun = false;
    public static string inSlotSun = null;

    public static bool slotSpot = false;
    public static string inSlotSpot = null;
}
