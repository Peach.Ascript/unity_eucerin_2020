﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    public AudioSource slot;
    [SerializeField] private Canvas canvas;
    private RectTransform rectTranform;
    private CanvasGroup canvasGroup;

    private string objectName;

    private int gameType = 0;
    Vector2 initPos = new Vector2();
    

    private void Awake()
    {
        rectTranform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();

        initPos = gameObject.transform.localPosition;
    }
    void Start()
    {
        gameType = 0;
        if (Confic.skinType == 1 || Confic.skinType == 2 || Confic.skinType == 3 ||
            Confic.skinType == 4 || Confic.skinType == 6)
        {
            gameType = 1;
        }
        if (Confic.skinType == 5)
        {
            gameType = 2;
        }
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("OnBeginDrag");
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
    }
    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("OnDrag");
        rectTranform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        //Debug.Log(gameObject.name);
        if (gameObject.name == "1" || gameObject.name == "2" || gameObject.name == "3" ||
            gameObject.name == "14" || gameObject.name == "15" || gameObject.name == "16")
        {
            this.GetComponent<RectTransform>().localScale = new Vector3(1.2f, 1.2f, 1.2f);
        }
        else if(gameObject.name == "4" || gameObject.name == "6" || gameObject.name == "7" || gameObject.name == "10")
        {
            this.GetComponent<RectTransform>().localScale = new Vector3(.96f, .96f, .96f);
        }
        else if (gameObject.name == "5" || gameObject.name == "11" || gameObject.name == "12" || gameObject.name == "13")
        {
            this.GetComponent<RectTransform>().localScale = new Vector3(1.08f, 1.08f, 1.08f);
        }
        else if (gameObject.name == "8" || gameObject.name == "9")
        {
            this.GetComponent<RectTransform>().localScale = new Vector3(.72f, .72f, .72f);
        }
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEndDrag"); 
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        slot.Play();

        if (Confic.slotCleanser == true)
        {
            objectName = gameObject.name;
            Confic.inSlotCleaser = objectName;
            GameObject itemC =  GameObject.Find(objectName);
            itemC.GetComponent<DragDrop>().enabled = false;
            itemC.GetComponent<RectTransform>().localPosition = new Vector3(-580, 370, 0);
            if (objectName == "5" || objectName == "6" || objectName == "10" || objectName == "12" || objectName == "13")
            {
                itemC.GetComponent<RectTransform>().localScale = new Vector3(.7f, .7f, .7f);
            }
            else if(itemC.GetComponent<RectTransform>().localScale.x > .9f)
            {
                itemC.GetComponent<RectTransform>().localScale = new Vector3(.9f, .9f, .9f);
            }
            //Debug.Log("cleanser" + objectName);
            Confic.slotCleanser = false;
        }
        else
        {
            gameObject.transform.localPosition = initPos;
        }
        if (Confic.slotMoistur == true && gameType == 1)
        {
            objectName = gameObject.name;
            Confic.inSlotMoistur = objectName;
            GameObject itemM = GameObject.Find(objectName);
            itemM.GetComponent<DragDrop>().enabled = false;
            itemM.GetComponent<RectTransform>().localPosition = new Vector3(-325, 100, 0);
            if (objectName == "5" || objectName == "6" || objectName == "10" || objectName == "12" || objectName == "13")
            {
                itemM.GetComponent<RectTransform>().localScale = new Vector3(.7f, .7f, .7f);
            }
            else if (itemM.GetComponent<RectTransform>().localScale.x > .9f)
            {
                itemM.GetComponent<RectTransform>().localScale = new Vector3(.9f, .9f, .9f);
            }
            //Debug.Log("moistur" + objectName);
            Confic.slotMoistur = false;
        }
        if (Confic.slotMoistur == true && gameType == 2)
        {
            objectName = gameObject.name;
            Confic.inSlotMoistur = objectName;
            GameObject itemM = GameObject.Find(objectName);
            itemM.GetComponent<DragDrop>().enabled = false;
            itemM.GetComponent<RectTransform>().localPosition = new Vector3(-325, 370, 0);
            if (objectName == "5" || objectName == "6" || objectName == "10" || objectName == "12" || objectName == "13")
            {
                itemM.GetComponent<RectTransform>().localScale = new Vector3(.7f, .7f, .7f);
            }
            else if (itemM.GetComponent<RectTransform>().localScale.x > .9f)
            {
                itemM.GetComponent<RectTransform>().localScale = new Vector3(.9f, .9f, .9f);
            }
            //Debug.Log("moistur" + objectName);
            Confic.slotMoistur = false;
        }
        if (Confic.slotSun == true && gameType == 1)
        {
            objectName = gameObject.name;
            Confic.inSlotSun = objectName;
            GameObject itemS = GameObject.Find(objectName);
            itemS.GetComponent<DragDrop>().enabled = false;
            itemS.GetComponent<RectTransform>().localPosition = new Vector3(-580, -180, 0);
            if (objectName == "5" || objectName == "6" || objectName == "10" || objectName == "12" || objectName == "13")
            {
                itemS.GetComponent<RectTransform>().localScale = new Vector3(.7f, .7f, .7f);
            }
            else if (itemS.GetComponent<RectTransform>().localScale.x > .9f)
            {
                itemS.GetComponent<RectTransform>().localScale = new Vector3(.9f, .9f, .9f);
            }
            //Debug.Log("sun" + objectName);
            Confic.slotSun = false;
        }
        if (Confic.slotSun == true && gameType == 2)
        {
            objectName = gameObject.name;
            Confic.inSlotSun = objectName;
            GameObject itemS = GameObject.Find(objectName);
            itemS.GetComponent<DragDrop>().enabled = false;
            itemS.GetComponent<RectTransform>().localPosition = new Vector3(-580, -180, 0);
            if (objectName == "5" || objectName == "6" || objectName == "10" || objectName == "12" || objectName == "13")
            {
                itemS.GetComponent<RectTransform>().localScale = new Vector3(.7f, .7f, .7f);
            }
            else if (itemS.GetComponent<RectTransform>().localScale.x > .9f)
            {
                itemS.GetComponent<RectTransform>().localScale = new Vector3(.9f, .9f, .9f);
            }
            //Debug.Log("sun" + objectName);
            Confic.slotSun = false;
        }
        if (Confic.slotSpot == true)
        {
            objectName = gameObject.name;
            Confic.inSlotSpot = objectName;
            GameObject itemSp = GameObject.Find(objectName);
            itemSp.GetComponent<DragDrop>().enabled = false;
            itemSp.GetComponent<RectTransform>().localPosition = new Vector3(-580, 90, 0);
            if (objectName == "5" || objectName == "6" || objectName == "10" || objectName == "12" || objectName == "13")
            {
                itemSp.GetComponent<RectTransform>().localScale = new Vector3(.7f, .7f, .7f);
            }
            else if (itemSp.GetComponent<RectTransform>().localScale.x > .9f)
            {
                itemSp.GetComponent<RectTransform>().localScale = new Vector3(.9f, .9f, .9f);
            }
            //Debug.Log("spot" + objectName);
            Confic.slotSpot = false;
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        //Debug.Log("OnPointerDown");
    }
    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log("drop"+gameObject.name);
    }
}
