﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InfoControler : MonoBehaviour
{
    public List<GameObject> info = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        for(int i =0; i< info.Count; i++)
        {
            info[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Confic.skinType == 1)
        {
            info[0].SetActive(true);
        }
        else if(Confic.skinType == 2)
        {
            info[1].SetActive(true);
        }
        else if (Confic.skinType == 3)
        {
            info[2].SetActive(true);
        }
        else if (Confic.skinType == 4)
        {
            info[3].SetActive(true);
        }
        else if (Confic.skinType == 5)
        {
            info[4].SetActive(true);
        }
        else if (Confic.skinType == 6)
        {
            info[5].SetActive(true);
        }
    }
    public void LoadScene(string sceneName)
    {
        StartCoroutine(ChangeScene(sceneName));
    }
    IEnumerator ChangeScene(string sceneName, float delay = .5f)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(sceneName);
    }
}
