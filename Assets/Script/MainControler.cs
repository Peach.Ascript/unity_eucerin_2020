﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainControler : MonoBehaviour
{
    public string gotoScene;
    public GameObject soundBg, soundBtn;

    private void Awake()
    {
        GameObject[] objs1 = GameObject.FindGameObjectsWithTag("musicBg");

        if (objs1.Length > 1)
        {
            for(int i = 1; i < objs1.Length; i++)
            {
                Destroy(objs1[i]);
            }
        }

        DontDestroyOnLoad(soundBg);
    }
    void Start()
    {     
        Confic.stateGame = 0;
        Confic.skinType = 0;

        Confic.submit = false;

        Confic.slotCleanser = false;
        Confic.inSlotCleaser = null;

        Confic.slotMoistur = false;
        Confic.inSlotMoistur = null;

        Confic.slotSun = false;
        Confic.inSlotSun = null;

        Confic.slotSpot = false;
        Confic.inSlotSpot = null;
}
    public void LoadScene(string sceneName)
    {
        StartCoroutine(ChangeScene(sceneName));
    }
    IEnumerator ChangeScene(string sceneName, float delay = .5f)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(sceneName);
    }

    public void SelectGame(int numGame)
    {
        switch (numGame)
        {
            case 1:
                Confic.skinType = 1;
                LoadScene(gotoScene);
                break;
            case 2:
                Confic.skinType = 2;
                LoadScene(gotoScene);
                break;
            case 3:
                Confic.skinType = 3;
                LoadScene(gotoScene);
                break;
            case 4:
                Confic.skinType = 4;
                LoadScene(gotoScene);
                break;
            case 5:
                Confic.skinType = 5;
                LoadScene(gotoScene);
                break;
            case 6:
                Confic.skinType = 6;
                LoadScene(gotoScene);
                break;
        }
    }
}
