﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainGame : MonoBehaviour
{
    public GameObject canvasType1, canvasType2, submitBtn, submitBtn2;
    [SerializeField]
    GameObject textTitle;

    // Start is called before the first frame update
    void Awake()
    {
        Confic.slotCleanser = false;
        Confic.inSlotCleaser = null;

        Confic.slotMoistur = false;
        Confic.inSlotMoistur = null;

        Confic.slotSun = false;
        Confic.inSlotSun = null;

        Confic.slotSpot = false;
        Confic.inSlotSpot = null;

        if (Confic.skinType == 1 || Confic.skinType == 2|| Confic.skinType == 3||
            Confic.skinType == 4|| Confic.skinType == 6)
        {
            canvasType1.SetActive(true);
            canvasType2.SetActive(false);
            if (Confic.skinType == 1)
            {
                textTitle.GetComponent<Text>().text = "Atopic Dematitis".ToString();   
            }
            else if (Confic.skinType == 2)
            {
                textTitle.GetComponent<Text>().text = "Dry and Cracked skin".ToString();
            }
            else if (Confic.skinType == 3)
            {
                textTitle.GetComponent<Text>().text = "Acne".ToString();
            }
            else if (Confic.skinType == 4)
            {
                textTitle.GetComponent<Text>().text = "Contact Dermatitis".ToString();
            }
            else if (Confic.skinType == 6)
            {
                textTitle.GetComponent<Text>().text = "AFTER LASER".ToString();
            }
        }
        if (Confic.skinType == 5)
        {
            canvasType1.SetActive(false);
            canvasType2.SetActive(true);
        }
    }
    void Update()
    {
        if (Confic.inSlotCleaser == null && Confic.inSlotMoistur == null && Confic.inSlotSun == null && Confic.inSlotSpot == null)
        {
            submitBtn.GetComponent<Button>().interactable = false;
            submitBtn2.GetComponent<Button>().interactable = false;
            submitBtn.GetComponent<Image>().color = new Color32(255, 255, 255, 180);
            submitBtn2.GetComponent<Image>().color = new Color32(255, 255, 255, 180);
        }
        else
        {
            submitBtn.GetComponent<Button>().interactable = true;
            submitBtn2.GetComponent<Button>().interactable = true;
            submitBtn.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            submitBtn2.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            Confic.submit = true;
        }
    }
    public void restart()
    {
        //SceneManager.LoadScene("03Game");
        StartCoroutine(ChangeScene("03Game"));
    }
    public void submit()
    {
        if(Confic.submit == true)
        {
            Confic.stateGame = 1;
            //SceneManager.LoadScene("04Submit");
            StartCoroutine(ChangeScene("04Submit"));
        }     
    }
    IEnumerator ChangeScene(string sceneName, float delay = .5f)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(sceneName);
    }
}
