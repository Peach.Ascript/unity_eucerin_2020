﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotSun : MonoBehaviour, IDropHandler
{
    bool onDroped = false;
    public void OnDrop(PointerEventData eventData)
    {
        if (onDroped) return;
        onDroped = true;
        //Debug.Log("Ondrop");
        Confic.slotSun = true;
        this.gameObject.GetComponent<RawImage>().texture = Resources.Load("page/page3/c0") as Texture2D;
    }
}
