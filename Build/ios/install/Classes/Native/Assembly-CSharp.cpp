﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// AfterGame
struct AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891;
// AfterGame/<ChangeScene>d__39
struct U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C;
// Confic
struct Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400;
// DragDrop
struct DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675;
// InfoControler
struct InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9;
// InfoControler/<ChangeScene>d__4
struct U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F;
// MainControler
struct MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8;
// MainControler/<ChangeScene>d__6
struct U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7;
// MainGame
struct MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72;
// MainGame/<ChangeScene>d__10
struct U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4;
// SlotCleanser
struct SlotCleanser_t935FCD20D87326A7812CB0B340762E1F797D4987;
// SlotMoistur
struct SlotMoistur_t6399F50D86CE9D2077A6730E3180A519FB5180C5;
// SlotSpotArea
struct SlotSpotArea_tA8A8FB3BFFBED826AA161E415B9C1070DBFE7833;
// SlotSun
struct SlotSun_t51356D9D4E1522336062D53A0D61E6AC3DC826D0;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t053DAB6E2110E276A0339D73497193F464BC1F82;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t3D4152882C54B77C712688E910390D5C8E030463;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8;

IL2CPP_EXTERN_C RuntimeClass* Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t3D4152882C54B77C712688E910390D5C8E030463_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral020A7BD137D302A5D7AEBB60C1EA77B8A4503434;
IL2CPP_EXTERN_C String_t* _stringLiteral0ADE7C2CF97F75D009975F4D720D1FA6C19F4897;
IL2CPP_EXTERN_C String_t* _stringLiteral1574BDDB75C78A6FD2251D61E2993B5146201319;
IL2CPP_EXTERN_C String_t* _stringLiteral17BA0791499DB908433B80F37C5FBC89B870084B;
IL2CPP_EXTERN_C String_t* _stringLiteral1B6453892473A467D07372D45EB05ABC2031647A;
IL2CPP_EXTERN_C String_t* _stringLiteral34B70FB2E9A7D5F879F77661E102E21A1EA349D2;
IL2CPP_EXTERN_C String_t* _stringLiteral3558E7A15B68B02593F56F3DCE046E82FBC5F540;
IL2CPP_EXTERN_C String_t* _stringLiteral356A192B7913B04C54574D18C28D46E6395428AB;
IL2CPP_EXTERN_C String_t* _stringLiteral5175C110028CFEEB857333006582230D49DF37FB;
IL2CPP_EXTERN_C String_t* _stringLiteral58243454356D7842EC6DB44B46D1E7DD93F8F094;
IL2CPP_EXTERN_C String_t* _stringLiteral5D055A20B62B7CAA1924E3AC313D52F853E8D1E2;
IL2CPP_EXTERN_C String_t* _stringLiteral66D86268C2DB6BE8C1C0C888C42741C953228575;
IL2CPP_EXTERN_C String_t* _stringLiteral70236C6C744FDD07EB3459FC64A09CDC1D7DA788;
IL2CPP_EXTERN_C String_t* _stringLiteral766C862EA4543A76F2D575E115DF348C13B0ABE5;
IL2CPP_EXTERN_C String_t* _stringLiteral77DE68DAECD823BABBB58EDB1C8E14D7106E83BB;
IL2CPP_EXTERN_C String_t* _stringLiteral7B52009B64FD0A2A49E6D8A939753077792B0554;
IL2CPP_EXTERN_C String_t* _stringLiteral807FBED80282E04DFDA8998F08C143FF2D73F591;
IL2CPP_EXTERN_C String_t* _stringLiteral902BA3CDA1883801594B6E1B452790CC53948FDA;
IL2CPP_EXTERN_C String_t* _stringLiteral90CC9BE4C49D3DFE4B74878E334E10721C4141F3;
IL2CPP_EXTERN_C String_t* _stringLiteral9E5C601A0DD0DE759E9F6A432BA8A08D7271C5B6;
IL2CPP_EXTERN_C String_t* _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4;
IL2CPP_EXTERN_C String_t* _stringLiteralB1D5781111D84F7B3FE45A0852E59758CD7A87E5;
IL2CPP_EXTERN_C String_t* _stringLiteralB3E73616A53F6724EB063E7E6C39BD7BF6FB2B7B;
IL2CPP_EXTERN_C String_t* _stringLiteralBD307A3EC329E10A2CFF8FB87480823DA114F8F4;
IL2CPP_EXTERN_C String_t* _stringLiteralBF7FE6E4F6BE348F80B339B934AF0432580C1236;
IL2CPP_EXTERN_C String_t* _stringLiteralC1DFD96EEA8CC2B62785275BCA38AC261256E278;
IL2CPP_EXTERN_C String_t* _stringLiteralD5553574F33C21CC970F71B86BD213C5C8ECC562;
IL2CPP_EXTERN_C String_t* _stringLiteralDA4B9237BACCCDF19C0760CAB7AEC4A8359010B0;
IL2CPP_EXTERN_C String_t* _stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C;
IL2CPP_EXTERN_C String_t* _stringLiteralF1ABD670358E036C31296E66B3B66C382AC00812;
IL2CPP_EXTERN_C String_t* _stringLiteralF2DD977A37B96AB4E037E48C20702C784EE30817;
IL2CPP_EXTERN_C String_t* _stringLiteralFA35E192121EABF3DABF9F5EA6ABDBCBC107AC3B;
IL2CPP_EXTERN_C String_t* _stringLiteralFE5DBBCEA5CE7E2988B8C69BCFDFDE8904AABC1F;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_m7E8ADD0E80ACBD9FACB9074DEE3565237F1A88AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mE0CF797BC1662A4FDFF8009E76AC0A5CD1BB1FCA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mA61C8F11E4F47F828FC3E31E04C25BF06F23B2A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_Reset_m5B4297928B22FE91DCDE0FD042ECADE85D23E59F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_Reset_mA9C68B0346703290FD417406AF4AD20248281FD3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_Reset_m3A7E4485FF2B48AD4A7628AE61F7DCAE8BDB3DAD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_Reset_m2FC6AAF41A7ABEF8C1AA8FABEFCDA00DCCAFD0FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AfterGame_AnsNull_m24DEEAE8E046C8162F4C79727B363504AEB978C6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AfterGame_Answer_mF50AFDC50177BBC66D74B15CC8D3A90581931554_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AfterGame_ChangeScene_m299E5279C8C9DE765D5D2400B1DA24086092DBC5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AfterGame_Done_m1DC3F3C8B03270661318E575908F2B947AEE0786_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AfterGame_Start_m7244FDB25E5B98EFA493A3E1B83DFBF86CD64522_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AfterGame__ctor_mF9663167CA6E12212C1A1E5AB30068D7094EC0F0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Confic__cctor_m27BD47F3EB837CDF1FF3A2844F6010336F066192_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DragDrop_Awake_m7B480CEE433ACC24AC26E9907300BD55344EA297_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DragDrop_OnDrag_m3F2E12B4B1977B305E53D9A5BFDECD05254833A2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DragDrop_OnEndDrag_m9FB4EF67A7C0316A93A73AD2DD0C6B8FB30E48FB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DragDrop_Start_m8B9764256A8BAE3C6CC26CDFA4196AF470AC57A8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InfoControler_ChangeScene_m347F912B0F276E3AB268E3563D905F575317AAAB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InfoControler_Start_m28D5FAC3FE3A3E780899D97B5E01AD68FE0EA513_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InfoControler_Update_m46184B20647AAEC95831C3CBF755420D2E5CA8AC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InfoControler__ctor_m88E6FCBF6F67C43ABB9792CDD02288F464AA7955_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainControler_Awake_m45D730272AF7272742302BF5F2BFE97DEA39F27E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainControler_ChangeScene_m16D7FC9E226ABC0D8D4BB94097067CB2ADDAC3C1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainControler_SelectGame_mB742DCA1262E691EF81FA220864E4E5E92F96D39_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainControler_Start_m526DA369C83B1398554DD8B6FB5E7072D13D6B99_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainGame_Awake_mDF3416E8B1FFA2206B605A56DBC967CACCD69C64_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainGame_ChangeScene_m4D0D38C5953215317DD0431939F4F2724E0E9292_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainGame_Update_m5806921D50C18B13698814FF871A5C2D47605895_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainGame_restart_m29651E434BC5E96E1AD0EC441C0602FF3A37B518_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainGame_submit_mE6C0F744EEF994D9BF3E0B92458C26A38165FD0B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SlotCleanser_OnDrop_m11F2388DC4A6D511D9F523C633FBE519F6FB3379_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SlotMoistur_OnDrop_m955C487F74BFAB6A675831FC4400223A155E6070_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SlotSpotArea_OnDrop_m7D7565151BC53C159635EDA55055FBB522DAA485_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SlotSun_OnDrop_m933329E9471F1DA381C7641B2683FF24F622E449_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CChangeSceneU3Ed__10_MoveNext_mEA9C7EC5BD998EB9033700AFB592D5621AFF0D00_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_Reset_m5B4297928B22FE91DCDE0FD042ECADE85D23E59F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CChangeSceneU3Ed__39_MoveNext_mC64851BA814A99FA1F5D63B5A94BB39E3C65FFF0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_Reset_mA9C68B0346703290FD417406AF4AD20248281FD3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CChangeSceneU3Ed__4_MoveNext_m894A21E18C7F0F9FA0834E4C7D2F4FE6D56D32BA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_Reset_m3A7E4485FF2B48AD4A7628AE61F7DCAE8BDB3DAD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CChangeSceneU3Ed__6_MoveNext_mA7244BBB01C847F0C8D322C9C620D61AE26636B7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_Reset_m2FC6AAF41A7ABEF8C1AA8FABEFCDA00DCCAFD0FB_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// System.Object


// AfterGame_<ChangeScene>d__39
struct  U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C  : public RuntimeObject
{
public:
	// System.Int32 AfterGame_<ChangeScene>d__39::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AfterGame_<ChangeScene>d__39::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single AfterGame_<ChangeScene>d__39::delay
	float ___delay_2;
	// System.String AfterGame_<ChangeScene>d__39::sceneName
	String_t* ___sceneName_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sceneName_3), (void*)value);
	}
};


// InfoControler_<ChangeScene>d__4
struct  U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F  : public RuntimeObject
{
public:
	// System.Int32 InfoControler_<ChangeScene>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object InfoControler_<ChangeScene>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single InfoControler_<ChangeScene>d__4::delay
	float ___delay_2;
	// System.String InfoControler_<ChangeScene>d__4::sceneName
	String_t* ___sceneName_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sceneName_3), (void*)value);
	}
};


// MainControler_<ChangeScene>d__6
struct  U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7  : public RuntimeObject
{
public:
	// System.Int32 MainControler_<ChangeScene>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MainControler_<ChangeScene>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single MainControler_<ChangeScene>d__6::delay
	float ___delay_2;
	// System.String MainControler_<ChangeScene>d__6::sceneName
	String_t* ___sceneName_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sceneName_3), (void*)value);
	}
};


// MainGame_<ChangeScene>d__10
struct  U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4  : public RuntimeObject
{
public:
	// System.Int32 MainGame_<ChangeScene>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MainGame_<ChangeScene>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single MainGame_<ChangeScene>d__10::delay
	float ___delay_2;
	// System.String MainGame_<ChangeScene>d__10::sceneName
	String_t* ___sceneName_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sceneName_3), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct  List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____items_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t3D4152882C54B77C712688E910390D5C8E030463  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3D4152882C54B77C712688E910390D5C8E030463, ____items_1)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3D4152882C54B77C712688E910390D5C8E030463, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3D4152882C54B77C712688E910390D5C8E030463, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3D4152882C54B77C712688E910390D5C8E030463, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3D4152882C54B77C712688E910390D5C8E030463_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3D4152882C54B77C712688E910390D5C8E030463_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};


// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5  : public AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5, ___m_EventSystem_1)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_1), (void*)value);
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_SelectedSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_3)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	float ___m_Seconds_0;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.EventSystems.PointerEventData_InputButton
struct  InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData_InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::displayIndex
	int32_t ___displayIndex_10;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___m_GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GameObject_0), (void*)value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___module_1)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___module_1), (void*)value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldNormal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___screenPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenPosition_9 = value;
	}

	inline static int32_t get_offset_of_displayIndex_10() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___displayIndex_10)); }
	inline int32_t get_displayIndex_10() const { return ___displayIndex_10; }
	inline int32_t* get_address_of_displayIndex_10() { return &___displayIndex_10; }
	inline void set_displayIndex_10(int32_t value)
	{
		___displayIndex_10 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_pinvoke
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
	int32_t ___displayIndex_10;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_com
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
	int32_t ___displayIndex_10;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_SelectedColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};


// UnityEngine.UI.Image_FillMethod
struct  FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5 
{
public:
	// System.Int32 UnityEngine.UI.Image_FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image_Type
struct  Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A 
{
public:
	// System.Int32 UnityEngine.UI.Image_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation_Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63  : public BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t3D4152882C54B77C712688E910390D5C8E030463 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData_InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerEnterU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___m_PointerPress_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointerPress_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClastPressU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrawPointerPressU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerDragU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_7))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerCurrentRaycastU3Ek__BackingField_7))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_8))->___m_GameObject_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CpointerPressRaycastU3Ek__BackingField_8))->___module_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___hovered_9)); }
	inline List_1_t3D4152882C54B77C712688E910390D5C8E030463 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t3D4152882C54B77C712688E910390D5C8E030463 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t3D4152882C54B77C712688E910390D5C8E030463 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hovered_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};

// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct  AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.Canvas
struct  Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields
{
public:
	// UnityEngine.Canvas_WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___willRenderCanvases_4), (void*)value);
	}
};


// UnityEngine.CanvasGroup
struct  CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20  : public Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA
{
public:

public:
};

struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_StaticFields
{
public:
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// AfterGame
struct  AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject AfterGame::Cleanser
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Cleanser_4;
	// UnityEngine.GameObject AfterGame::Moistur
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Moistur_5;
	// UnityEngine.GameObject AfterGame::Sun
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Sun_6;
	// UnityEngine.GameObject AfterGame::textTitle1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textTitle1_7;
	// UnityEngine.GameObject AfterGame::textInfo1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textInfo1_8;
	// UnityEngine.GameObject AfterGame::textTitle2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textTitle2_9;
	// UnityEngine.GameObject AfterGame::textInfo2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textInfo2_10;
	// UnityEngine.GameObject AfterGame::textTitle3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textTitle3_11;
	// UnityEngine.GameObject AfterGame::textInfo3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textInfo3_12;
	// UnityEngine.GameObject AfterGame::textTitle4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textTitle4_13;
	// UnityEngine.GameObject AfterGame::textInfo4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textInfo4_14;
	// UnityEngine.GameObject AfterGame::textTitle6
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textTitle6_15;
	// UnityEngine.GameObject AfterGame::textInfo6
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textInfo6_16;
	// UnityEngine.GameObject AfterGame::ansCleanser
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ansCleanser_17;
	// UnityEngine.GameObject AfterGame::ansMoistur
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ansMoistur_18;
	// UnityEngine.GameObject AfterGame::ansSun
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ansSun_19;
	// UnityEngine.GameObject AfterGame::imgCleanser
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___imgCleanser_20;
	// UnityEngine.GameObject AfterGame::imgMoistur
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___imgMoistur_21;
	// UnityEngine.GameObject AfterGame::imgSun
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___imgSun_22;
	// UnityEngine.UI.Text AfterGame::textC
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textC_23;
	// UnityEngine.UI.Text AfterGame::textM
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textM_24;
	// UnityEngine.UI.Text AfterGame::textS
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textS_25;
	// UnityEngine.UI.Text AfterGame::textM2
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textM2_26;
	// UnityEngine.UI.Text AfterGame::textSp
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textSp_27;
	// UnityEngine.UI.Text AfterGame::textS2
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textS2_28;
	// UnityEngine.GameObject AfterGame::canvasType1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvasType1_29;
	// UnityEngine.GameObject AfterGame::canvasType2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvasType2_30;
	// UnityEngine.GameObject AfterGame::canvasType3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvasType3_31;
	// UnityEngine.GameObject AfterGame::canvasType4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvasType4_32;
	// UnityEngine.GameObject AfterGame::canvasType5
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvasType5_33;
	// UnityEngine.GameObject AfterGame::canvasType6
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvasType6_34;
	// UnityEngine.GameObject AfterGame::ansMoistur2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ansMoistur2_35;
	// UnityEngine.GameObject AfterGame::ansSpot2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ansSpot2_36;
	// UnityEngine.GameObject AfterGame::ansSun2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ansSun2_37;
	// System.Collections.Generic.List`1<System.String> AfterGame::product
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___product_38;

public:
	inline static int32_t get_offset_of_Cleanser_4() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___Cleanser_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Cleanser_4() const { return ___Cleanser_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Cleanser_4() { return &___Cleanser_4; }
	inline void set_Cleanser_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Cleanser_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Cleanser_4), (void*)value);
	}

	inline static int32_t get_offset_of_Moistur_5() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___Moistur_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Moistur_5() const { return ___Moistur_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Moistur_5() { return &___Moistur_5; }
	inline void set_Moistur_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Moistur_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Moistur_5), (void*)value);
	}

	inline static int32_t get_offset_of_Sun_6() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___Sun_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Sun_6() const { return ___Sun_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Sun_6() { return &___Sun_6; }
	inline void set_Sun_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Sun_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Sun_6), (void*)value);
	}

	inline static int32_t get_offset_of_textTitle1_7() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textTitle1_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textTitle1_7() const { return ___textTitle1_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textTitle1_7() { return &___textTitle1_7; }
	inline void set_textTitle1_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textTitle1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textTitle1_7), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo1_8() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textInfo1_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textInfo1_8() const { return ___textInfo1_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textInfo1_8() { return &___textInfo1_8; }
	inline void set_textInfo1_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textInfo1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo1_8), (void*)value);
	}

	inline static int32_t get_offset_of_textTitle2_9() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textTitle2_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textTitle2_9() const { return ___textTitle2_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textTitle2_9() { return &___textTitle2_9; }
	inline void set_textTitle2_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textTitle2_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textTitle2_9), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo2_10() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textInfo2_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textInfo2_10() const { return ___textInfo2_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textInfo2_10() { return &___textInfo2_10; }
	inline void set_textInfo2_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textInfo2_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo2_10), (void*)value);
	}

	inline static int32_t get_offset_of_textTitle3_11() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textTitle3_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textTitle3_11() const { return ___textTitle3_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textTitle3_11() { return &___textTitle3_11; }
	inline void set_textTitle3_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textTitle3_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textTitle3_11), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo3_12() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textInfo3_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textInfo3_12() const { return ___textInfo3_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textInfo3_12() { return &___textInfo3_12; }
	inline void set_textInfo3_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textInfo3_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo3_12), (void*)value);
	}

	inline static int32_t get_offset_of_textTitle4_13() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textTitle4_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textTitle4_13() const { return ___textTitle4_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textTitle4_13() { return &___textTitle4_13; }
	inline void set_textTitle4_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textTitle4_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textTitle4_13), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo4_14() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textInfo4_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textInfo4_14() const { return ___textInfo4_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textInfo4_14() { return &___textInfo4_14; }
	inline void set_textInfo4_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textInfo4_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo4_14), (void*)value);
	}

	inline static int32_t get_offset_of_textTitle6_15() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textTitle6_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textTitle6_15() const { return ___textTitle6_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textTitle6_15() { return &___textTitle6_15; }
	inline void set_textTitle6_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textTitle6_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textTitle6_15), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo6_16() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textInfo6_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textInfo6_16() const { return ___textInfo6_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textInfo6_16() { return &___textInfo6_16; }
	inline void set_textInfo6_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textInfo6_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo6_16), (void*)value);
	}

	inline static int32_t get_offset_of_ansCleanser_17() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___ansCleanser_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ansCleanser_17() const { return ___ansCleanser_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ansCleanser_17() { return &___ansCleanser_17; }
	inline void set_ansCleanser_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ansCleanser_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ansCleanser_17), (void*)value);
	}

	inline static int32_t get_offset_of_ansMoistur_18() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___ansMoistur_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ansMoistur_18() const { return ___ansMoistur_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ansMoistur_18() { return &___ansMoistur_18; }
	inline void set_ansMoistur_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ansMoistur_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ansMoistur_18), (void*)value);
	}

	inline static int32_t get_offset_of_ansSun_19() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___ansSun_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ansSun_19() const { return ___ansSun_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ansSun_19() { return &___ansSun_19; }
	inline void set_ansSun_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ansSun_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ansSun_19), (void*)value);
	}

	inline static int32_t get_offset_of_imgCleanser_20() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___imgCleanser_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_imgCleanser_20() const { return ___imgCleanser_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_imgCleanser_20() { return &___imgCleanser_20; }
	inline void set_imgCleanser_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___imgCleanser_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imgCleanser_20), (void*)value);
	}

	inline static int32_t get_offset_of_imgMoistur_21() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___imgMoistur_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_imgMoistur_21() const { return ___imgMoistur_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_imgMoistur_21() { return &___imgMoistur_21; }
	inline void set_imgMoistur_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___imgMoistur_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imgMoistur_21), (void*)value);
	}

	inline static int32_t get_offset_of_imgSun_22() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___imgSun_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_imgSun_22() const { return ___imgSun_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_imgSun_22() { return &___imgSun_22; }
	inline void set_imgSun_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___imgSun_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imgSun_22), (void*)value);
	}

	inline static int32_t get_offset_of_textC_23() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textC_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textC_23() const { return ___textC_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textC_23() { return &___textC_23; }
	inline void set_textC_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textC_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textC_23), (void*)value);
	}

	inline static int32_t get_offset_of_textM_24() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textM_24)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textM_24() const { return ___textM_24; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textM_24() { return &___textM_24; }
	inline void set_textM_24(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textM_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textM_24), (void*)value);
	}

	inline static int32_t get_offset_of_textS_25() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textS_25)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textS_25() const { return ___textS_25; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textS_25() { return &___textS_25; }
	inline void set_textS_25(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textS_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textS_25), (void*)value);
	}

	inline static int32_t get_offset_of_textM2_26() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textM2_26)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textM2_26() const { return ___textM2_26; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textM2_26() { return &___textM2_26; }
	inline void set_textM2_26(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textM2_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textM2_26), (void*)value);
	}

	inline static int32_t get_offset_of_textSp_27() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textSp_27)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textSp_27() const { return ___textSp_27; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textSp_27() { return &___textSp_27; }
	inline void set_textSp_27(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textSp_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textSp_27), (void*)value);
	}

	inline static int32_t get_offset_of_textS2_28() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___textS2_28)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textS2_28() const { return ___textS2_28; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textS2_28() { return &___textS2_28; }
	inline void set_textS2_28(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textS2_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textS2_28), (void*)value);
	}

	inline static int32_t get_offset_of_canvasType1_29() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___canvasType1_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvasType1_29() const { return ___canvasType1_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvasType1_29() { return &___canvasType1_29; }
	inline void set_canvasType1_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvasType1_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasType1_29), (void*)value);
	}

	inline static int32_t get_offset_of_canvasType2_30() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___canvasType2_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvasType2_30() const { return ___canvasType2_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvasType2_30() { return &___canvasType2_30; }
	inline void set_canvasType2_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvasType2_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasType2_30), (void*)value);
	}

	inline static int32_t get_offset_of_canvasType3_31() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___canvasType3_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvasType3_31() const { return ___canvasType3_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvasType3_31() { return &___canvasType3_31; }
	inline void set_canvasType3_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvasType3_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasType3_31), (void*)value);
	}

	inline static int32_t get_offset_of_canvasType4_32() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___canvasType4_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvasType4_32() const { return ___canvasType4_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvasType4_32() { return &___canvasType4_32; }
	inline void set_canvasType4_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvasType4_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasType4_32), (void*)value);
	}

	inline static int32_t get_offset_of_canvasType5_33() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___canvasType5_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvasType5_33() const { return ___canvasType5_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvasType5_33() { return &___canvasType5_33; }
	inline void set_canvasType5_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvasType5_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasType5_33), (void*)value);
	}

	inline static int32_t get_offset_of_canvasType6_34() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___canvasType6_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvasType6_34() const { return ___canvasType6_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvasType6_34() { return &___canvasType6_34; }
	inline void set_canvasType6_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvasType6_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasType6_34), (void*)value);
	}

	inline static int32_t get_offset_of_ansMoistur2_35() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___ansMoistur2_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ansMoistur2_35() const { return ___ansMoistur2_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ansMoistur2_35() { return &___ansMoistur2_35; }
	inline void set_ansMoistur2_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ansMoistur2_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ansMoistur2_35), (void*)value);
	}

	inline static int32_t get_offset_of_ansSpot2_36() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___ansSpot2_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ansSpot2_36() const { return ___ansSpot2_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ansSpot2_36() { return &___ansSpot2_36; }
	inline void set_ansSpot2_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ansSpot2_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ansSpot2_36), (void*)value);
	}

	inline static int32_t get_offset_of_ansSun2_37() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___ansSun2_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ansSun2_37() const { return ___ansSun2_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ansSun2_37() { return &___ansSun2_37; }
	inline void set_ansSun2_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ansSun2_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ansSun2_37), (void*)value);
	}

	inline static int32_t get_offset_of_product_38() { return static_cast<int32_t>(offsetof(AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891, ___product_38)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_product_38() const { return ___product_38; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_product_38() { return &___product_38; }
	inline void set_product_38(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___product_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___product_38), (void*)value);
	}
};


// Confic
struct  Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields
{
public:
	// System.Int32 Confic::stateGame
	int32_t ___stateGame_4;
	// System.Int32 Confic::skinType
	int32_t ___skinType_5;
	// System.Boolean Confic::submit
	bool ___submit_6;
	// System.Boolean Confic::imgSlotC
	bool ___imgSlotC_7;
	// System.Boolean Confic::imgSlotM
	bool ___imgSlotM_8;
	// System.Boolean Confic::slotCleanser
	bool ___slotCleanser_9;
	// System.String Confic::inSlotCleaser
	String_t* ___inSlotCleaser_10;
	// System.Boolean Confic::slotMoistur
	bool ___slotMoistur_11;
	// System.String Confic::inSlotMoistur
	String_t* ___inSlotMoistur_12;
	// System.Boolean Confic::slotSun
	bool ___slotSun_13;
	// System.String Confic::inSlotSun
	String_t* ___inSlotSun_14;
	// System.Boolean Confic::slotSpot
	bool ___slotSpot_15;
	// System.String Confic::inSlotSpot
	String_t* ___inSlotSpot_16;

public:
	inline static int32_t get_offset_of_stateGame_4() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___stateGame_4)); }
	inline int32_t get_stateGame_4() const { return ___stateGame_4; }
	inline int32_t* get_address_of_stateGame_4() { return &___stateGame_4; }
	inline void set_stateGame_4(int32_t value)
	{
		___stateGame_4 = value;
	}

	inline static int32_t get_offset_of_skinType_5() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___skinType_5)); }
	inline int32_t get_skinType_5() const { return ___skinType_5; }
	inline int32_t* get_address_of_skinType_5() { return &___skinType_5; }
	inline void set_skinType_5(int32_t value)
	{
		___skinType_5 = value;
	}

	inline static int32_t get_offset_of_submit_6() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___submit_6)); }
	inline bool get_submit_6() const { return ___submit_6; }
	inline bool* get_address_of_submit_6() { return &___submit_6; }
	inline void set_submit_6(bool value)
	{
		___submit_6 = value;
	}

	inline static int32_t get_offset_of_imgSlotC_7() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___imgSlotC_7)); }
	inline bool get_imgSlotC_7() const { return ___imgSlotC_7; }
	inline bool* get_address_of_imgSlotC_7() { return &___imgSlotC_7; }
	inline void set_imgSlotC_7(bool value)
	{
		___imgSlotC_7 = value;
	}

	inline static int32_t get_offset_of_imgSlotM_8() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___imgSlotM_8)); }
	inline bool get_imgSlotM_8() const { return ___imgSlotM_8; }
	inline bool* get_address_of_imgSlotM_8() { return &___imgSlotM_8; }
	inline void set_imgSlotM_8(bool value)
	{
		___imgSlotM_8 = value;
	}

	inline static int32_t get_offset_of_slotCleanser_9() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___slotCleanser_9)); }
	inline bool get_slotCleanser_9() const { return ___slotCleanser_9; }
	inline bool* get_address_of_slotCleanser_9() { return &___slotCleanser_9; }
	inline void set_slotCleanser_9(bool value)
	{
		___slotCleanser_9 = value;
	}

	inline static int32_t get_offset_of_inSlotCleaser_10() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___inSlotCleaser_10)); }
	inline String_t* get_inSlotCleaser_10() const { return ___inSlotCleaser_10; }
	inline String_t** get_address_of_inSlotCleaser_10() { return &___inSlotCleaser_10; }
	inline void set_inSlotCleaser_10(String_t* value)
	{
		___inSlotCleaser_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inSlotCleaser_10), (void*)value);
	}

	inline static int32_t get_offset_of_slotMoistur_11() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___slotMoistur_11)); }
	inline bool get_slotMoistur_11() const { return ___slotMoistur_11; }
	inline bool* get_address_of_slotMoistur_11() { return &___slotMoistur_11; }
	inline void set_slotMoistur_11(bool value)
	{
		___slotMoistur_11 = value;
	}

	inline static int32_t get_offset_of_inSlotMoistur_12() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___inSlotMoistur_12)); }
	inline String_t* get_inSlotMoistur_12() const { return ___inSlotMoistur_12; }
	inline String_t** get_address_of_inSlotMoistur_12() { return &___inSlotMoistur_12; }
	inline void set_inSlotMoistur_12(String_t* value)
	{
		___inSlotMoistur_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inSlotMoistur_12), (void*)value);
	}

	inline static int32_t get_offset_of_slotSun_13() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___slotSun_13)); }
	inline bool get_slotSun_13() const { return ___slotSun_13; }
	inline bool* get_address_of_slotSun_13() { return &___slotSun_13; }
	inline void set_slotSun_13(bool value)
	{
		___slotSun_13 = value;
	}

	inline static int32_t get_offset_of_inSlotSun_14() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___inSlotSun_14)); }
	inline String_t* get_inSlotSun_14() const { return ___inSlotSun_14; }
	inline String_t** get_address_of_inSlotSun_14() { return &___inSlotSun_14; }
	inline void set_inSlotSun_14(String_t* value)
	{
		___inSlotSun_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inSlotSun_14), (void*)value);
	}

	inline static int32_t get_offset_of_slotSpot_15() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___slotSpot_15)); }
	inline bool get_slotSpot_15() const { return ___slotSpot_15; }
	inline bool* get_address_of_slotSpot_15() { return &___slotSpot_15; }
	inline void set_slotSpot_15(bool value)
	{
		___slotSpot_15 = value;
	}

	inline static int32_t get_offset_of_inSlotSpot_16() { return static_cast<int32_t>(offsetof(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields, ___inSlotSpot_16)); }
	inline String_t* get_inSlotSpot_16() const { return ___inSlotSpot_16; }
	inline String_t** get_address_of_inSlotSpot_16() { return &___inSlotSpot_16; }
	inline void set_inSlotSpot_16(String_t* value)
	{
		___inSlotSpot_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inSlotSpot_16), (void*)value);
	}
};


// DragDrop
struct  DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource DragDrop::slot
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___slot_4;
	// UnityEngine.Canvas DragDrop::canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___canvas_5;
	// UnityEngine.RectTransform DragDrop::rectTranform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rectTranform_6;
	// UnityEngine.CanvasGroup DragDrop::canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___canvasGroup_7;
	// System.String DragDrop::objectName
	String_t* ___objectName_8;
	// System.Int32 DragDrop::gameType
	int32_t ___gameType_9;
	// UnityEngine.Vector2 DragDrop::initPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___initPos_10;

public:
	inline static int32_t get_offset_of_slot_4() { return static_cast<int32_t>(offsetof(DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675, ___slot_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_slot_4() const { return ___slot_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_slot_4() { return &___slot_4; }
	inline void set_slot_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___slot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slot_4), (void*)value);
	}

	inline static int32_t get_offset_of_canvas_5() { return static_cast<int32_t>(offsetof(DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675, ___canvas_5)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_canvas_5() const { return ___canvas_5; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_canvas_5() { return &___canvas_5; }
	inline void set_canvas_5(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___canvas_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvas_5), (void*)value);
	}

	inline static int32_t get_offset_of_rectTranform_6() { return static_cast<int32_t>(offsetof(DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675, ___rectTranform_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rectTranform_6() const { return ___rectTranform_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rectTranform_6() { return &___rectTranform_6; }
	inline void set_rectTranform_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rectTranform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rectTranform_6), (void*)value);
	}

	inline static int32_t get_offset_of_canvasGroup_7() { return static_cast<int32_t>(offsetof(DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675, ___canvasGroup_7)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_canvasGroup_7() const { return ___canvasGroup_7; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_canvasGroup_7() { return &___canvasGroup_7; }
	inline void set_canvasGroup_7(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___canvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasGroup_7), (void*)value);
	}

	inline static int32_t get_offset_of_objectName_8() { return static_cast<int32_t>(offsetof(DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675, ___objectName_8)); }
	inline String_t* get_objectName_8() const { return ___objectName_8; }
	inline String_t** get_address_of_objectName_8() { return &___objectName_8; }
	inline void set_objectName_8(String_t* value)
	{
		___objectName_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectName_8), (void*)value);
	}

	inline static int32_t get_offset_of_gameType_9() { return static_cast<int32_t>(offsetof(DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675, ___gameType_9)); }
	inline int32_t get_gameType_9() const { return ___gameType_9; }
	inline int32_t* get_address_of_gameType_9() { return &___gameType_9; }
	inline void set_gameType_9(int32_t value)
	{
		___gameType_9 = value;
	}

	inline static int32_t get_offset_of_initPos_10() { return static_cast<int32_t>(offsetof(DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675, ___initPos_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_initPos_10() const { return ___initPos_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_initPos_10() { return &___initPos_10; }
	inline void set_initPos_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___initPos_10 = value;
	}
};


// InfoControler
struct  InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> InfoControler::info
	List_1_t3D4152882C54B77C712688E910390D5C8E030463 * ___info_4;

public:
	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9, ___info_4)); }
	inline List_1_t3D4152882C54B77C712688E910390D5C8E030463 * get_info_4() const { return ___info_4; }
	inline List_1_t3D4152882C54B77C712688E910390D5C8E030463 ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(List_1_t3D4152882C54B77C712688E910390D5C8E030463 * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___info_4), (void*)value);
	}
};


// MainControler
struct  MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String MainControler::gotoScene
	String_t* ___gotoScene_4;
	// UnityEngine.GameObject MainControler::soundBg
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___soundBg_5;
	// UnityEngine.GameObject MainControler::soundBtn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___soundBtn_6;

public:
	inline static int32_t get_offset_of_gotoScene_4() { return static_cast<int32_t>(offsetof(MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8, ___gotoScene_4)); }
	inline String_t* get_gotoScene_4() const { return ___gotoScene_4; }
	inline String_t** get_address_of_gotoScene_4() { return &___gotoScene_4; }
	inline void set_gotoScene_4(String_t* value)
	{
		___gotoScene_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gotoScene_4), (void*)value);
	}

	inline static int32_t get_offset_of_soundBg_5() { return static_cast<int32_t>(offsetof(MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8, ___soundBg_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_soundBg_5() const { return ___soundBg_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_soundBg_5() { return &___soundBg_5; }
	inline void set_soundBg_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___soundBg_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___soundBg_5), (void*)value);
	}

	inline static int32_t get_offset_of_soundBtn_6() { return static_cast<int32_t>(offsetof(MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8, ___soundBtn_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_soundBtn_6() const { return ___soundBtn_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_soundBtn_6() { return &___soundBtn_6; }
	inline void set_soundBtn_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___soundBtn_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___soundBtn_6), (void*)value);
	}
};


// MainGame
struct  MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MainGame::canvasType1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvasType1_4;
	// UnityEngine.GameObject MainGame::canvasType2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvasType2_5;
	// UnityEngine.GameObject MainGame::submitBtn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___submitBtn_6;
	// UnityEngine.GameObject MainGame::submitBtn2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___submitBtn2_7;
	// UnityEngine.GameObject MainGame::textTitle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textTitle_8;
	// UnityEngine.GameObject MainGame::imgInfo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___imgInfo_9;

public:
	inline static int32_t get_offset_of_canvasType1_4() { return static_cast<int32_t>(offsetof(MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72, ___canvasType1_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvasType1_4() const { return ___canvasType1_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvasType1_4() { return &___canvasType1_4; }
	inline void set_canvasType1_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvasType1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasType1_4), (void*)value);
	}

	inline static int32_t get_offset_of_canvasType2_5() { return static_cast<int32_t>(offsetof(MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72, ___canvasType2_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvasType2_5() const { return ___canvasType2_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvasType2_5() { return &___canvasType2_5; }
	inline void set_canvasType2_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvasType2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasType2_5), (void*)value);
	}

	inline static int32_t get_offset_of_submitBtn_6() { return static_cast<int32_t>(offsetof(MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72, ___submitBtn_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_submitBtn_6() const { return ___submitBtn_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_submitBtn_6() { return &___submitBtn_6; }
	inline void set_submitBtn_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___submitBtn_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submitBtn_6), (void*)value);
	}

	inline static int32_t get_offset_of_submitBtn2_7() { return static_cast<int32_t>(offsetof(MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72, ___submitBtn2_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_submitBtn2_7() const { return ___submitBtn2_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_submitBtn2_7() { return &___submitBtn2_7; }
	inline void set_submitBtn2_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___submitBtn2_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submitBtn2_7), (void*)value);
	}

	inline static int32_t get_offset_of_textTitle_8() { return static_cast<int32_t>(offsetof(MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72, ___textTitle_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textTitle_8() const { return ___textTitle_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textTitle_8() { return &___textTitle_8; }
	inline void set_textTitle_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textTitle_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textTitle_8), (void*)value);
	}

	inline static int32_t get_offset_of_imgInfo_9() { return static_cast<int32_t>(offsetof(MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72, ___imgInfo_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_imgInfo_9() const { return ___imgInfo_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_imgInfo_9() { return &___imgInfo_9; }
	inline void set_imgInfo_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___imgInfo_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imgInfo_9), (void*)value);
	}
};


// SlotCleanser
struct  SlotCleanser_t935FCD20D87326A7812CB0B340762E1F797D4987  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean SlotCleanser::onDroped
	bool ___onDroped_4;

public:
	inline static int32_t get_offset_of_onDroped_4() { return static_cast<int32_t>(offsetof(SlotCleanser_t935FCD20D87326A7812CB0B340762E1F797D4987, ___onDroped_4)); }
	inline bool get_onDroped_4() const { return ___onDroped_4; }
	inline bool* get_address_of_onDroped_4() { return &___onDroped_4; }
	inline void set_onDroped_4(bool value)
	{
		___onDroped_4 = value;
	}
};


// SlotMoistur
struct  SlotMoistur_t6399F50D86CE9D2077A6730E3180A519FB5180C5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean SlotMoistur::onDroped
	bool ___onDroped_4;

public:
	inline static int32_t get_offset_of_onDroped_4() { return static_cast<int32_t>(offsetof(SlotMoistur_t6399F50D86CE9D2077A6730E3180A519FB5180C5, ___onDroped_4)); }
	inline bool get_onDroped_4() const { return ___onDroped_4; }
	inline bool* get_address_of_onDroped_4() { return &___onDroped_4; }
	inline void set_onDroped_4(bool value)
	{
		___onDroped_4 = value;
	}
};


// SlotSpotArea
struct  SlotSpotArea_tA8A8FB3BFFBED826AA161E415B9C1070DBFE7833  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean SlotSpotArea::onDroped
	bool ___onDroped_4;

public:
	inline static int32_t get_offset_of_onDroped_4() { return static_cast<int32_t>(offsetof(SlotSpotArea_tA8A8FB3BFFBED826AA161E415B9C1070DBFE7833, ___onDroped_4)); }
	inline bool get_onDroped_4() const { return ___onDroped_4; }
	inline bool* get_address_of_onDroped_4() { return &___onDroped_4; }
	inline void set_onDroped_4(bool value)
	{
		___onDroped_4 = value;
	}
};


// SlotSun
struct  SlotSun_t51356D9D4E1522336062D53A0D61E6AC3DC826D0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean SlotSun::onDroped
	bool ___onDroped_4;

public:
	inline static int32_t get_offset_of_onDroped_4() { return static_cast<int32_t>(offsetof(SlotSun_t51356D9D4E1522336062D53A0D61E6AC3DC826D0, ___onDroped_4)); }
	inline bool get_onDroped_4() const { return ___onDroped_4; }
	inline bool* get_address_of_onDroped_4() { return &___onDroped_4; }
	inline void set_onDroped_4(bool value)
	{
		___onDroped_4 = value;
	}
};


// UnityEngine.AudioSource
struct  AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C  : public AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7
{
public:

public:
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_11;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_12;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_13;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_14;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_18;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_CachedMesh_21;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_CachedUvs_22;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_23;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_11() const { return ___m_RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_11() { return &___m_RectTransform_11; }
	inline void set_m_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_12)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_12() const { return ___m_CanvasRenderer_12; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_12() { return &___m_CanvasRenderer_12; }
	inline void set_m_CanvasRenderer_12(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_13)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_14)); }
	inline bool get_m_VertsDirty_14() const { return ___m_VertsDirty_14; }
	inline bool* get_address_of_m_VertsDirty_14() { return &___m_VertsDirty_14; }
	inline void set_m_VertsDirty_14(bool value)
	{
		___m_VertsDirty_14 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_15)); }
	inline bool get_m_MaterialDirty_15() const { return ___m_MaterialDirty_15; }
	inline bool* get_address_of_m_MaterialDirty_15() { return &___m_MaterialDirty_15; }
	inline void set_m_MaterialDirty_15(bool value)
	{
		___m_MaterialDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_16() const { return ___m_OnDirtyLayoutCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_16() { return &___m_OnDirtyLayoutCallback_16; }
	inline void set_m_OnDirtyLayoutCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_17)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_17() const { return ___m_OnDirtyVertsCallback_17; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_17() { return &___m_OnDirtyVertsCallback_17; }
	inline void set_m_OnDirtyVertsCallback_17(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_18)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_18() const { return ___m_OnDirtyMaterialCallback_18; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_18() { return &___m_OnDirtyMaterialCallback_18; }
	inline void set_m_OnDirtyMaterialCallback_18(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_21() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_CachedMesh_21() const { return ___m_CachedMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_CachedMesh_21() { return &___m_CachedMesh_21; }
	inline void set_m_CachedMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_CachedMesh_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_22() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedUvs_22)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_CachedUvs_22() const { return ___m_CachedUvs_22; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_CachedUvs_22() { return &___m_CachedUvs_22; }
	inline void set_m_CachedUvs_22(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_CachedUvs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_23() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_23)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_23() const { return ___m_ColorTweenRunner_23; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_23() { return &___m_ColorTweenRunner_23; }
	inline void set_m_ColorTweenRunner_23(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_24(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_24 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_19;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_20;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_19() const { return ___s_Mesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_19() { return &___s_Mesh_19; }
	inline void set_s_Mesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_20)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_20() const { return ___s_VertexHelper_20; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_20() { return &___s_VertexHelper_20; }
	inline void set_s_VertexHelper_20(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_20), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_7;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Boolean UnityEngine.UI.Selectable::m_WillRemove
	bool ___m_WillRemove_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_7)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_4), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_9)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_10)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_13)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_WillRemove_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_WillRemove_15)); }
	inline bool get_m_WillRemove_15() const { return ___m_WillRemove_15; }
	inline bool* get_address_of_m_WillRemove_15() { return &___m_WillRemove_15; }
	inline void set_m_WillRemove_15(bool value)
	{
		___m_WillRemove_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_19)); }
	inline List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;
	// System.Boolean UnityEngine.UI.Selectable::s_IsDirty
	bool ___s_IsDirty_6;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}

	inline static int32_t get_offset_of_s_IsDirty_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_IsDirty_6)); }
	inline bool get_s_IsDirty_6() const { return ___s_IsDirty_6; }
	inline bool* get_address_of_s_IsDirty_6() { return &___s_IsDirty_6; }
	inline void set_s_IsDirty_6(bool value)
	{
		___s_IsDirty_6 = value;
	}
};


// UnityEngine.UI.Button
struct  Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.UI.Button_ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B, ___m_OnClick_20)); }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_25;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_26;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_27;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_29;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_31;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_32;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_33;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_25)); }
	inline bool get_m_ShouldRecalculateStencil_25() const { return ___m_ShouldRecalculateStencil_25; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_25() { return &___m_ShouldRecalculateStencil_25; }
	inline void set_m_ShouldRecalculateStencil_25(bool value)
	{
		___m_ShouldRecalculateStencil_25 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_26() const { return ___m_MaskMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_26() { return &___m_MaskMaterial_26; }
	inline void set_m_MaskMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_27)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_27() const { return ___m_ParentMask_27; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_27() { return &___m_ParentMask_27; }
	inline void set_m_ParentMask_27(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_28)); }
	inline bool get_m_Maskable_28() const { return ___m_Maskable_28; }
	inline bool* get_address_of_m_Maskable_28() { return &___m_Maskable_28; }
	inline void set_m_Maskable_28(bool value)
	{
		___m_Maskable_28 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_29)); }
	inline bool get_m_IncludeForMasking_29() const { return ___m_IncludeForMasking_29; }
	inline bool* get_address_of_m_IncludeForMasking_29() { return &___m_IncludeForMasking_29; }
	inline void set_m_IncludeForMasking_29(bool value)
	{
		___m_IncludeForMasking_29 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_30)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_30() const { return ___m_OnCullStateChanged_30; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_30() { return &___m_OnCullStateChanged_30; }
	inline void set_m_OnCullStateChanged_30(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_31)); }
	inline bool get_m_ShouldRecalculate_31() const { return ___m_ShouldRecalculate_31; }
	inline bool* get_address_of_m_ShouldRecalculate_31() { return &___m_ShouldRecalculate_31; }
	inline void set_m_ShouldRecalculate_31(bool value)
	{
		___m_ShouldRecalculate_31 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_32)); }
	inline int32_t get_m_StencilValue_32() const { return ___m_StencilValue_32; }
	inline int32_t* get_address_of_m_StencilValue_32() { return &___m_StencilValue_32; }
	inline void set_m_StencilValue_32(int32_t value)
	{
		___m_StencilValue_32 = value;
	}

	inline static int32_t get_offset_of_m_Corners_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_33)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_33() const { return ___m_Corners_33; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_33() { return &___m_Corners_33; }
	inline void set_m_Corners_33(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_33), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_Sprite_35;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_OverrideSprite_36;
	// UnityEngine.UI.Image_Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_37;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_38;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_39;
	// UnityEngine.UI.Image_FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_40;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_41;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_42;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_43;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_44;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_45;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_46;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_47;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_48;

public:
	inline static int32_t get_offset_of_m_Sprite_35() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Sprite_35)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_Sprite_35() const { return ___m_Sprite_35; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_Sprite_35() { return &___m_Sprite_35; }
	inline void set_m_Sprite_35(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_Sprite_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_36() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_OverrideSprite_36)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_OverrideSprite_36() const { return ___m_OverrideSprite_36; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_OverrideSprite_36() { return &___m_OverrideSprite_36; }
	inline void set_m_OverrideSprite_36(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_OverrideSprite_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_37() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Type_37)); }
	inline int32_t get_m_Type_37() const { return ___m_Type_37; }
	inline int32_t* get_address_of_m_Type_37() { return &___m_Type_37; }
	inline void set_m_Type_37(int32_t value)
	{
		___m_Type_37 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_38() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PreserveAspect_38)); }
	inline bool get_m_PreserveAspect_38() const { return ___m_PreserveAspect_38; }
	inline bool* get_address_of_m_PreserveAspect_38() { return &___m_PreserveAspect_38; }
	inline void set_m_PreserveAspect_38(bool value)
	{
		___m_PreserveAspect_38 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_39() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillCenter_39)); }
	inline bool get_m_FillCenter_39() const { return ___m_FillCenter_39; }
	inline bool* get_address_of_m_FillCenter_39() { return &___m_FillCenter_39; }
	inline void set_m_FillCenter_39(bool value)
	{
		___m_FillCenter_39 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_40() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillMethod_40)); }
	inline int32_t get_m_FillMethod_40() const { return ___m_FillMethod_40; }
	inline int32_t* get_address_of_m_FillMethod_40() { return &___m_FillMethod_40; }
	inline void set_m_FillMethod_40(int32_t value)
	{
		___m_FillMethod_40 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_41() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillAmount_41)); }
	inline float get_m_FillAmount_41() const { return ___m_FillAmount_41; }
	inline float* get_address_of_m_FillAmount_41() { return &___m_FillAmount_41; }
	inline void set_m_FillAmount_41(float value)
	{
		___m_FillAmount_41 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_42() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillClockwise_42)); }
	inline bool get_m_FillClockwise_42() const { return ___m_FillClockwise_42; }
	inline bool* get_address_of_m_FillClockwise_42() { return &___m_FillClockwise_42; }
	inline void set_m_FillClockwise_42(bool value)
	{
		___m_FillClockwise_42 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_43() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillOrigin_43)); }
	inline int32_t get_m_FillOrigin_43() const { return ___m_FillOrigin_43; }
	inline int32_t* get_address_of_m_FillOrigin_43() { return &___m_FillOrigin_43; }
	inline void set_m_FillOrigin_43(int32_t value)
	{
		___m_FillOrigin_43 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_44() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_AlphaHitTestMinimumThreshold_44)); }
	inline float get_m_AlphaHitTestMinimumThreshold_44() const { return ___m_AlphaHitTestMinimumThreshold_44; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_44() { return &___m_AlphaHitTestMinimumThreshold_44; }
	inline void set_m_AlphaHitTestMinimumThreshold_44(float value)
	{
		___m_AlphaHitTestMinimumThreshold_44 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_45() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Tracked_45)); }
	inline bool get_m_Tracked_45() const { return ___m_Tracked_45; }
	inline bool* get_address_of_m_Tracked_45() { return &___m_Tracked_45; }
	inline void set_m_Tracked_45(bool value)
	{
		___m_Tracked_45 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_46() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_UseSpriteMesh_46)); }
	inline bool get_m_UseSpriteMesh_46() const { return ___m_UseSpriteMesh_46; }
	inline bool* get_address_of_m_UseSpriteMesh_46() { return &___m_UseSpriteMesh_46; }
	inline void set_m_UseSpriteMesh_46(bool value)
	{
		___m_UseSpriteMesh_46 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_47() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PixelsPerUnitMultiplier_47)); }
	inline float get_m_PixelsPerUnitMultiplier_47() const { return ___m_PixelsPerUnitMultiplier_47; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_47() { return &___m_PixelsPerUnitMultiplier_47; }
	inline void set_m_PixelsPerUnitMultiplier_47(float value)
	{
		___m_PixelsPerUnitMultiplier_47 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_48() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_CachedReferencePixelsPerUnit_48)); }
	inline float get_m_CachedReferencePixelsPerUnit_48() const { return ___m_CachedReferencePixelsPerUnit_48; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_48() { return &___m_CachedReferencePixelsPerUnit_48; }
	inline void set_m_CachedReferencePixelsPerUnit_48(float value)
	{
		___m_CachedReferencePixelsPerUnit_48 = value;
	}
};

struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_ETC1DefaultUI_34;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_VertScratch_49;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_UVScratch_50;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Xy_51;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Uv_52;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * ___m_TrackedTexturelessImages_53;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_54;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_34() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_ETC1DefaultUI_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_ETC1DefaultUI_34() const { return ___s_ETC1DefaultUI_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_ETC1DefaultUI_34() { return &___s_ETC1DefaultUI_34; }
	inline void set_s_ETC1DefaultUI_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_ETC1DefaultUI_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_34), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_49() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_VertScratch_49)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_VertScratch_49() const { return ___s_VertScratch_49; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_VertScratch_49() { return &___s_VertScratch_49; }
	inline void set_s_VertScratch_49(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_VertScratch_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_49), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_50() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_UVScratch_50)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_UVScratch_50() const { return ___s_UVScratch_50; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_UVScratch_50() { return &___s_UVScratch_50; }
	inline void set_s_UVScratch_50(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_UVScratch_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_50), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_51() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Xy_51)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Xy_51() const { return ___s_Xy_51; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Xy_51() { return &___s_Xy_51; }
	inline void set_s_Xy_51(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Xy_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_52() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Uv_52)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Uv_52() const { return ___s_Uv_52; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Uv_52() { return &___s_Uv_52; }
	inline void set_s_Uv_52(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Uv_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_52), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_53() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___m_TrackedTexturelessImages_53)); }
	inline List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * get_m_TrackedTexturelessImages_53() const { return ___m_TrackedTexturelessImages_53; }
	inline List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA ** get_address_of_m_TrackedTexturelessImages_53() { return &___m_TrackedTexturelessImages_53; }
	inline void set_m_TrackedTexturelessImages_53(List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * value)
	{
		___m_TrackedTexturelessImages_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_54() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Initialized_54)); }
	inline bool get_s_Initialized_54() const { return ___s_Initialized_54; }
	inline bool* get_address_of_s_Initialized_54() { return &___s_Initialized_54; }
	inline void set_s_Initialized_54(bool value)
	{
		___s_Initialized_54 = value;
	}
};


// UnityEngine.UI.RawImage
struct  RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Texture_34;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_UVRect_35;

public:
	inline static int32_t get_offset_of_m_Texture_34() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_Texture_34)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_Texture_34() const { return ___m_Texture_34; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_Texture_34() { return &___m_Texture_34; }
	inline void set_m_Texture_34(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_Texture_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Texture_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_UVRect_35() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_UVRect_35)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_UVRect_35() const { return ___m_UVRect_35; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_UVRect_35() { return &___m_UVRect_35; }
	inline void set_m_UVRect_35(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_UVRect_35 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_34;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_35;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_36;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_37;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_39;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_40;

public:
	inline static int32_t get_offset_of_m_FontData_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_34)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_34() const { return ___m_FontData_34; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_34() { return &___m_FontData_34; }
	inline void set_m_FontData_34(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_35)); }
	inline String_t* get_m_Text_35() const { return ___m_Text_35; }
	inline String_t** get_address_of_m_Text_35() { return &___m_Text_35; }
	inline void set_m_Text_35(String_t* value)
	{
		___m_Text_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_36)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_36() const { return ___m_TextCache_36; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_36() { return &___m_TextCache_36; }
	inline void set_m_TextCache_36(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_37() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_37)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_37() const { return ___m_TextCacheForLayout_37; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_37() { return &___m_TextCacheForLayout_37; }
	inline void set_m_TextCacheForLayout_37(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_39() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_39)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_39() const { return ___m_DisableFontTextureRebuiltCallback_39; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_39() { return &___m_DisableFontTextureRebuiltCallback_39; }
	inline void set_m_DisableFontTextureRebuiltCallback_39(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_39 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_40() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_40)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_40() const { return ___m_TempVerts_40; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_40() { return &___m_TempVerts_40; }
	inline void set_m_TempVerts_40(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_40), (void*)value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_38;

public:
	inline static int32_t get_offset_of_s_DefaultText_38() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_38)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_38() const { return ___s_DefaultText_38; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_38() { return &___s_DefaultText_38; }
	inline void set_s_DefaultText_38(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_38), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * m_Items[1];

public:
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);

// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.RawImage>()
inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1 (String_t* ___path0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9 (RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Collections.IEnumerator AfterGame::ChangeScene(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AfterGame_ChangeScene_m299E5279C8C9DE765D5D2400B1DA24086092DBC5 (AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891 * __this, String_t* ___sceneName0, float ___delay1, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void AfterGame/<ChangeScene>d__39::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__39__ctor_mB6C44C3BA8154033CF3387AF0A689A5B730AB036 (U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559 (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CanvasGroup>()
inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * Component_GetComponent_TisCanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_m7E8ADD0E80ACBD9FACB9074DEE3565237F1A88AA (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CanvasGroup_set_alpha_m7E3C4DCD13E6B1FD43C797EFF9698BACA1FBEC3D (CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CanvasGroup_set_blocksRaycasts_m94D595956A88573219381E8F79A2B7CBEBCE7B10 (CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  RectTransform_get_anchoredPosition_mCB2171DBADBC572F354CCFE3ACA19F9506F97907 (RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_delta()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  PointerEventData_get_delta_mC5D62E985D40A7708316C6E07B699B96D9C8184E_inline (PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Canvas::get_scaleFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Canvas_get_scaleFactor_m0F6D59E75F7605ABD2AFF6AF32A1097226CE060A (Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchoredPosition_m4DD45DB1A97734A1F3A81E5F259638ECAF35962F (RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD (String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<DragDrop>()
inline DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_inline (List_1_t3D4152882C54B77C712688E910390D5C8E030463 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * (*) (List_1_t3D4152882C54B77C712688E910390D5C8E030463 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count()
inline int32_t List_1_get_Count_mA61C8F11E4F47F828FC3E31E04C25BF06F23B2A4_inline (List_1_t3D4152882C54B77C712688E910390D5C8E030463 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3D4152882C54B77C712688E910390D5C8E030463 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Collections.IEnumerator InfoControler::ChangeScene(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InfoControler_ChangeScene_m347F912B0F276E3AB268E3563D905F575317AAAB (InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9 * __this, String_t* ___sceneName0, float ___delay1, const RuntimeMethod* method);
// System.Void InfoControler/<ChangeScene>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__4__ctor_mF840D806063E826EA7BD5155C96F1E1FD036A3A1 (U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
inline void List_1__ctor_mE0CF797BC1662A4FDFF8009E76AC0A5CD1BB1FCA (List_1_t3D4152882C54B77C712688E910390D5C8E030463 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3D4152882C54B77C712688E910390D5C8E030463 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* GameObject_FindGameObjectsWithTag_mF49A195F19A598C5FD145FFE175ABE5B4885FAD9 (String_t* ___tag0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___target0, const RuntimeMethod* method);
// System.Collections.IEnumerator MainControler::ChangeScene(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MainControler_ChangeScene_m16D7FC9E226ABC0D8D4BB94097067CB2ADDAC3C1 (MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8 * __this, String_t* ___sceneName0, float ___delay1, const RuntimeMethod* method);
// System.Void MainControler/<ChangeScene>d__6::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__6__ctor_m0456867FD259B715C25732F93DB08B0EC6AF4E75 (U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void MainControler::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381 (MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8 * __this, String_t* ___sceneName0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable_set_interactable_mF0897CD627B603DE1F3714FFD8B121AB694E0B6B (Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2 (Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61 (Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___c0, const RuntimeMethod* method);
// System.Collections.IEnumerator MainGame::ChangeScene(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MainGame_ChangeScene_m4D0D38C5953215317DD0431939F4F2724E0E9292 (MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72 * __this, String_t* ___sceneName0, float ___delay1, const RuntimeMethod* method);
// System.Void MainGame/<ChangeScene>d__10::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__10__ctor_mBB94D50E14BDD3FA29E34EC6586438B6E618B3D3 (U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AfterGame::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterGame_Start_m7244FDB25E5B98EFA493A3E1B83DFBF86CD64522 (AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterGame_Start_m7244FDB25E5B98EFA493A3E1B83DFBF86CD64522_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0085;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = __this->get_textTitle1_7();
		NullCheck(L_1);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_2 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_1, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteralBF7FE6E4F6BE348F80B339B934AF0432580C1236);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteralBF7FE6E4F6BE348F80B339B934AF0432580C1236);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_textInfo1_8();
		NullCheck(L_4);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_5 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_4, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = __this->get_canvasType1_29();
		NullCheck(L_7);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_7, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = __this->get_canvasType2_30();
		NullCheck(L_8);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_8, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = __this->get_canvasType3_31();
		NullCheck(L_9);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_canvasType4_32();
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = __this->get_canvasType5_33();
		NullCheck(L_11);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_11, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = __this->get_canvasType6_34();
		NullCheck(L_12);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_12, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_13 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_13) == ((uint32_t)2))))
		{
			goto IL_010a;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_textTitle2_9();
		NullCheck(L_14);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_15 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_14, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteral766C862EA4543A76F2D575E115DF348C13B0ABE5);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral766C862EA4543A76F2D575E115DF348C13B0ABE5);
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, L_16);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = __this->get_textInfo2_10();
		NullCheck(L_17);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_18 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_17, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_19);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = __this->get_canvasType1_29();
		NullCheck(L_20);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_20, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_21 = __this->get_canvasType2_30();
		NullCheck(L_21);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_21, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = __this->get_canvasType3_31();
		NullCheck(L_22);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_22, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_23 = __this->get_canvasType4_32();
		NullCheck(L_23);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_23, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_24 = __this->get_canvasType5_33();
		NullCheck(L_24);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_24, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_25 = __this->get_canvasType6_34();
		NullCheck(L_25);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_25, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_010a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_26 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_26) == ((uint32_t)3))))
		{
			goto IL_018f;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_27 = __this->get_textTitle3_11();
		NullCheck(L_27);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_28 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_27, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteral90CC9BE4C49D3DFE4B74878E334E10721C4141F3);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral90CC9BE4C49D3DFE4B74878E334E10721C4141F3);
		NullCheck(L_28);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_28, L_29);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_30 = __this->get_textInfo3_12();
		NullCheck(L_30);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_31 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_30, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		NullCheck(L_31);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_31, L_32);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_33 = __this->get_canvasType1_29();
		NullCheck(L_33);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_33, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_34 = __this->get_canvasType2_30();
		NullCheck(L_34);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_34, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_35 = __this->get_canvasType3_31();
		NullCheck(L_35);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_35, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_36 = __this->get_canvasType4_32();
		NullCheck(L_36);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_36, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_37 = __this->get_canvasType5_33();
		NullCheck(L_37);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_37, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_38 = __this->get_canvasType6_34();
		NullCheck(L_38);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_38, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_018f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_39 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_39) == ((uint32_t)4))))
		{
			goto IL_0214;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_40 = __this->get_textTitle4_13();
		NullCheck(L_40);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_41 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_40, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteral5D055A20B62B7CAA1924E3AC313D52F853E8D1E2);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral5D055A20B62B7CAA1924E3AC313D52F853E8D1E2);
		NullCheck(L_41);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, L_42);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_43 = __this->get_textInfo4_14();
		NullCheck(L_43);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_44 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_43, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		NullCheck(L_44);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_44, L_45);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_46 = __this->get_canvasType1_29();
		NullCheck(L_46);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_46, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_47 = __this->get_canvasType2_30();
		NullCheck(L_47);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_47, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_48 = __this->get_canvasType3_31();
		NullCheck(L_48);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_48, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_49 = __this->get_canvasType4_32();
		NullCheck(L_49);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_49, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_50 = __this->get_canvasType5_33();
		NullCheck(L_50);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_50, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_51 = __this->get_canvasType6_34();
		NullCheck(L_51);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_51, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0214:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_52 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_52) == ((uint32_t)5))))
		{
			goto IL_0265;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_53 = __this->get_canvasType1_29();
		NullCheck(L_53);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_53, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_54 = __this->get_canvasType2_30();
		NullCheck(L_54);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_54, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_55 = __this->get_canvasType3_31();
		NullCheck(L_55);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_55, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_56 = __this->get_canvasType4_32();
		NullCheck(L_56);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_56, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_57 = __this->get_canvasType5_33();
		NullCheck(L_57);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_57, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_58 = __this->get_canvasType6_34();
		NullCheck(L_58);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_58, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0265:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_59 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_59) == ((uint32_t)6))))
		{
			goto IL_02e9;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_60 = __this->get_textTitle6_15();
		NullCheck(L_60);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_61 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_60, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteral58243454356D7842EC6DB44B46D1E7DD93F8F094);
		String_t* L_62 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral58243454356D7842EC6DB44B46D1E7DD93F8F094);
		NullCheck(L_61);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_61, L_62);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_63 = __this->get_textInfo6_16();
		NullCheck(L_63);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_64 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_63, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		String_t* L_65 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteralE655F6F1E8119EAB018A4E8FB3D7152B1BBDC98C);
		NullCheck(L_64);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_64, L_65);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_66 = __this->get_canvasType1_29();
		NullCheck(L_66);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_66, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_67 = __this->get_canvasType2_30();
		NullCheck(L_67);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_67, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_68 = __this->get_canvasType3_31();
		NullCheck(L_68);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_68, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_69 = __this->get_canvasType4_32();
		NullCheck(L_69);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_69, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_70 = __this->get_canvasType5_33();
		NullCheck(L_70);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_70, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_71 = __this->get_canvasType6_34();
		NullCheck(L_71);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_71, (bool)1, /*hidden argument*/NULL);
	}

IL_02e9:
	{
		return;
	}
}
// System.Void AfterGame::Answer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterGame_Answer_mF50AFDC50177BBC66D74B15CC8D3A90581931554 (AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterGame_Answer_mF50AFDC50177BBC66D74B15CC8D3A90581931554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_ansCleanser_17();
		NullCheck(L_0);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_1 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_0, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_2 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotCleaser_10();
		String_t* L_3 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralB3E73616A53F6724EB063E7E6C39BD7BF6FB2B7B, L_2, /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_4 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_1, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_4, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_ansCleanser_17();
		NullCheck(L_5);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_6 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_5, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = __this->get_ansMoistur_18();
		NullCheck(L_7);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_8 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_7, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		String_t* L_9 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotMoistur_12();
		String_t* L_10 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralB3E73616A53F6724EB063E7E6C39BD7BF6FB2B7B, L_9, /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_11 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_8, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_11, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = __this->get_ansMoistur_18();
		NullCheck(L_12);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_13 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_12, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_13);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_ansSun_19();
		NullCheck(L_14);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_15 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_14, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		String_t* L_16 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSun_14();
		String_t* L_17 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralB3E73616A53F6724EB063E7E6C39BD7BF6FB2B7B, L_16, /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_18 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_15, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_18, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_19 = __this->get_ansSun_19();
		NullCheck(L_19);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_20 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_19, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_20);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_21 = __this->get_ansMoistur2_35();
		NullCheck(L_21);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_22 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_21, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		String_t* L_23 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotMoistur_12();
		String_t* L_24 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralB3E73616A53F6724EB063E7E6C39BD7BF6FB2B7B, L_23, /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_25 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_22, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_25, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = __this->get_ansMoistur2_35();
		NullCheck(L_26);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_27 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_26, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		NullCheck(L_27);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_27);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_28 = __this->get_ansSpot2_36();
		NullCheck(L_28);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_29 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_28, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		String_t* L_30 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSpot_16();
		String_t* L_31 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralB3E73616A53F6724EB063E7E6C39BD7BF6FB2B7B, L_30, /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_32 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(L_31, /*hidden argument*/NULL);
		NullCheck(L_29);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_29, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_32, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_33 = __this->get_ansSpot2_36();
		NullCheck(L_33);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_34 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_33, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		NullCheck(L_34);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_34);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_35 = __this->get_ansSun2_37();
		NullCheck(L_35);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_36 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_35, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		String_t* L_37 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSun_14();
		String_t* L_38 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralB3E73616A53F6724EB063E7E6C39BD7BF6FB2B7B, L_37, /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_39 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(L_38, /*hidden argument*/NULL);
		NullCheck(L_36);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_36, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_39, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_40 = __this->get_ansSun2_37();
		NullCheck(L_40);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_41 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_40, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		NullCheck(L_41);
		VirtActionInvoker0::Invoke(45 /* System.Void UnityEngine.UI.Graphic::SetNativeSize() */, L_41);
		return;
	}
}
// System.Void AfterGame::AnsNull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterGame_AnsNull_m24DEEAE8E046C8162F4C79727B363504AEB978C6 (AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterGame_AnsNull_m24DEEAE8E046C8162F4C79727B363504AEB978C6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotCleaser_10();
		if (L_0)
		{
			goto IL_005b;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = __this->get_ansCleanser_17();
		NullCheck(L_1);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = __this->get_ansMoistur_18();
		NullCheck(L_2);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_3 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_2, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_4), (-250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_3, L_4, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_ansSun_19();
		NullCheck(L_5);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_6 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_5, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), (250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_6, L_7, /*hidden argument*/NULL);
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_8 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotMoistur_12();
		if (L_8)
		{
			goto IL_010d;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = __this->get_ansMoistur_18();
		NullCheck(L_9);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_ansMoistur2_35();
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = __this->get_ansCleanser_17();
		NullCheck(L_11);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_12 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_11, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_13), (-250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_12, L_13, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_ansSun_19();
		NullCheck(L_14);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_15 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_14, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_16), (250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_15, L_16, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = __this->get_ansSpot2_36();
		NullCheck(L_17);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_18 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_17, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19;
		memset((&L_19), 0, sizeof(L_19));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_19), (-250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_18, L_19, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = __this->get_ansSun2_37();
		NullCheck(L_20);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_21 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_20, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_22), (250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_21, L_22, /*hidden argument*/NULL);
	}

IL_010d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_23 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSun_14();
		if (L_23)
		{
			goto IL_01bf;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_24 = __this->get_ansSun_19();
		NullCheck(L_24);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_24, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_25 = __this->get_ansSun2_37();
		NullCheck(L_25);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_25, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = __this->get_ansCleanser_17();
		NullCheck(L_26);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_27 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_26, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_28), (-250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_27, L_28, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = __this->get_ansMoistur_18();
		NullCheck(L_29);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_30 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_29, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31;
		memset((&L_31), 0, sizeof(L_31));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_31), (250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_30, L_31, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = __this->get_ansMoistur2_35();
		NullCheck(L_32);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_33 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_32, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34;
		memset((&L_34), 0, sizeof(L_34));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_34), (-250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_33, L_34, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_35 = __this->get_ansSpot2_36();
		NullCheck(L_35);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_36 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_35, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37;
		memset((&L_37), 0, sizeof(L_37));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_37), (250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_36, L_37, /*hidden argument*/NULL);
	}

IL_01bf:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_38 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSpot_16();
		if (L_38)
		{
			goto IL_021a;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_39 = __this->get_ansSun2_37();
		NullCheck(L_39);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_39, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_40 = __this->get_ansMoistur2_35();
		NullCheck(L_40);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_41 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_40, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_42), (-250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_41, L_42, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_43 = __this->get_ansSun2_37();
		NullCheck(L_43);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_44 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_43, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45;
		memset((&L_45), 0, sizeof(L_45));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_45), (250.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_44, L_45, /*hidden argument*/NULL);
	}

IL_021a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_46 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotCleaser_10();
		if (L_46)
		{
			goto IL_024c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_47 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotMoistur_12();
		if (L_47)
		{
			goto IL_024c;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_48 = __this->get_ansSun_19();
		NullCheck(L_48);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_49 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_48, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_50;
		memset((&L_50), 0, sizeof(L_50));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_50), (0.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_49, L_50, /*hidden argument*/NULL);
	}

IL_024c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_51 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotMoistur_12();
		if (L_51)
		{
			goto IL_027e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_52 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSun_14();
		if (L_52)
		{
			goto IL_027e;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_53 = __this->get_ansCleanser_17();
		NullCheck(L_53);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_54 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_53, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55;
		memset((&L_55), 0, sizeof(L_55));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_55), (0.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_54, L_55, /*hidden argument*/NULL);
	}

IL_027e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_56 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotCleaser_10();
		if (L_56)
		{
			goto IL_02b0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_57 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSun_14();
		if (L_57)
		{
			goto IL_02b0;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_58 = __this->get_ansMoistur_18();
		NullCheck(L_58);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_59 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_58, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60;
		memset((&L_60), 0, sizeof(L_60));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_60), (0.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_59);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_59, L_60, /*hidden argument*/NULL);
	}

IL_02b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_61 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSpot_16();
		if (L_61)
		{
			goto IL_02e2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_62 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSun_14();
		if (L_62)
		{
			goto IL_02e2;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_63 = __this->get_ansMoistur2_35();
		NullCheck(L_63);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_64 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_63, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_65;
		memset((&L_65), 0, sizeof(L_65));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_65), (0.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_64);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_64, L_65, /*hidden argument*/NULL);
	}

IL_02e2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_66 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSpot_16();
		if (L_66)
		{
			goto IL_0314;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_67 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotMoistur_12();
		if (L_67)
		{
			goto IL_0314;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_68 = __this->get_ansSun2_37();
		NullCheck(L_68);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_69 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_68, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_70;
		memset((&L_70), 0, sizeof(L_70));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_70), (0.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_69);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_69, L_70, /*hidden argument*/NULL);
	}

IL_0314:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_71 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSun_14();
		if (L_71)
		{
			goto IL_0346;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_72 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotMoistur_12();
		if (L_72)
		{
			goto IL_0346;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_73 = __this->get_ansSpot2_36();
		NullCheck(L_73);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_74 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_73, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_75;
		memset((&L_75), 0, sizeof(L_75));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_75), (0.0f), (-312.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_74, L_75, /*hidden argument*/NULL);
	}

IL_0346:
	{
		return;
	}
}
// System.Void AfterGame::Done()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterGame_Done_m1DC3F3C8B03270661318E575908F2B947AEE0786 (AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterGame_Done_m1DC3F3C8B03270661318E575908F2B947AEE0786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = AfterGame_ChangeScene_m299E5279C8C9DE765D5D2400B1DA24086092DBC5(__this, _stringLiteral5175C110028CFEEB857333006582230D49DF37FB, (0.5f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AfterGame::ChangeScene(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AfterGame_ChangeScene_m299E5279C8C9DE765D5D2400B1DA24086092DBC5 (AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891 * __this, String_t* ___sceneName0, float ___delay1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterGame_ChangeScene_m299E5279C8C9DE765D5D2400B1DA24086092DBC5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * L_0 = (U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C *)il2cpp_codegen_object_new(U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C_il2cpp_TypeInfo_var);
		U3CChangeSceneU3Ed__39__ctor_mB6C44C3BA8154033CF3387AF0A689A5B730AB036(L_0, 0, /*hidden argument*/NULL);
		U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * L_1 = L_0;
		String_t* L_2 = ___sceneName0;
		NullCheck(L_1);
		L_1->set_sceneName_3(L_2);
		U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * L_3 = L_1;
		float L_4 = ___delay1;
		NullCheck(L_3);
		L_3->set_delay_2(L_4);
		return L_3;
	}
}
// System.Void AfterGame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterGame__ctor_mF9663167CA6E12212C1A1E5AB30068D7094EC0F0 (AfterGame_t13D1F2325B6465084A138A8431323A446DFAB891 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterGame__ctor_mF9663167CA6E12212C1A1E5AB30068D7094EC0F0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_0, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		__this->set_product_38(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AfterGame_<ChangeScene>d__39::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__39__ctor_mB6C44C3BA8154033CF3387AF0A689A5B730AB036 (U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void AfterGame_<ChangeScene>d__39::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__39_System_IDisposable_Dispose_m2D912323752DEA50F6E88C0DA0B73F74F78B84FB (U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean AfterGame_<ChangeScene>d__39::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CChangeSceneU3Ed__39_MoveNext_mC64851BA814A99FA1F5D63B5A94BB39E3C65FFF0 (U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeSceneU3Ed__39_MoveNext_mC64851BA814A99FA1F5D63B5A94BB39E3C65FFF0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = __this->get_delay_2();
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_4 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0031:
	{
		__this->set_U3CU3E1__state_0((-1));
		String_t* L_5 = __this->get_sceneName_3();
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(L_5, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object AfterGame_<ChangeScene>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeSceneU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF9C3477C108A663FC89AFFBA540CED2F4CBD441 (U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void AfterGame_<ChangeScene>d__39::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_Reset_mA9C68B0346703290FD417406AF4AD20248281FD3 (U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_Reset_mA9C68B0346703290FD417406AF4AD20248281FD3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_Reset_mA9C68B0346703290FD417406AF4AD20248281FD3_RuntimeMethod_var);
	}
}
// System.Object AfterGame_<ChangeScene>d__39::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_get_Current_mABC25FC239B5A0C82674BF0E1C35A4E71E13A410 (U3CChangeSceneU3Ed__39_t0AA2E42A040F234EF7C7EFD3572CBE67BDE8712C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Confic::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Confic__ctor_mA0EE2BF9E6BA062E0D35AF050AE8EEE0ED65B677 (Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Confic::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Confic__cctor_m27BD47F3EB837CDF1FF3A2844F6010336F066192 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Confic__cctor_m27BD47F3EB837CDF1FF3A2844F6010336F066192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_stateGame_4(0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_skinType_5(0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_submit_6((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_imgSlotC_7((bool)1);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_imgSlotM_8((bool)1);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotCleanser_9((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotCleaser_10((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotMoistur_11((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotMoistur_12((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSun_13((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSun_14((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSpot_15((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSpot_16((String_t*)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DragDrop::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragDrop_Awake_m7B480CEE433ACC24AC26E9907300BD55344EA297 (DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragDrop_Awake_m7B480CEE433ACC24AC26E9907300BD55344EA297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_0 = Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103_RuntimeMethod_var);
		__this->set_rectTranform_6(L_0);
		CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * L_1 = Component_GetComponent_TisCanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_m7E8ADD0E80ACBD9FACB9074DEE3565237F1A88AA(__this, /*hidden argument*/Component_GetComponent_TisCanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_m7E8ADD0E80ACBD9FACB9074DEE3565237F1A88AA_RuntimeMethod_var);
		__this->set_canvasGroup_7(L_1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_4, /*hidden argument*/NULL);
		__this->set_initPos_10(L_5);
		return;
	}
}
// System.Void DragDrop::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragDrop_Start_m8B9764256A8BAE3C6CC26CDFA4196AF470AC57A8 (DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragDrop_Start_m8B9764256A8BAE3C6CC26CDFA4196AF470AC57A8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_gameType_9(0);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_2 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((((int32_t)L_3) == ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_4 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_4) == ((uint32_t)6))))
		{
			goto IL_0036;
		}
	}

IL_002f:
	{
		__this->set_gameType_9(1);
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_5 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)5))))
		{
			goto IL_0045;
		}
	}
	{
		__this->set_gameType_9(2);
	}

IL_0045:
	{
		return;
	}
}
// System.Void DragDrop::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragDrop_OnBeginDrag_mC2894502E70CDF5A5F8639C211BE4D73658F8A71 (DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	{
		CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * L_0 = __this->get_canvasGroup_7();
		NullCheck(L_0);
		CanvasGroup_set_alpha_m7E3C4DCD13E6B1FD43C797EFF9698BACA1FBEC3D(L_0, (0.6f), /*hidden argument*/NULL);
		CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * L_1 = __this->get_canvasGroup_7();
		NullCheck(L_1);
		CanvasGroup_set_blocksRaycasts_m94D595956A88573219381E8F79A2B7CBEBCE7B10(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragDrop::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragDrop_OnDrag_m3F2E12B4B1977B305E53D9A5BFDECD05254833A2 (DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragDrop_OnDrag_m3F2E12B4B1977B305E53D9A5BFDECD05254833A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_0 = __this->get_rectTranform_6();
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_1 = L_0;
		NullCheck(L_1);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = RectTransform_get_anchoredPosition_mCB2171DBADBC572F354CCFE3ACA19F9506F97907(L_1, /*hidden argument*/NULL);
		PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * L_3 = ___eventData0;
		NullCheck(L_3);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = PointerEventData_get_delta_mC5D62E985D40A7708316C6E07B699B96D9C8184E_inline(L_3, /*hidden argument*/NULL);
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_5 = __this->get_canvas_5();
		NullCheck(L_5);
		float L_6 = Canvas_get_scaleFactor_m0F6D59E75F7605ABD2AFF6AF32A1097226CE060A(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_4, L_6, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_2, L_7, /*hidden argument*/NULL);
		NullCheck(L_1);
		RectTransform_set_anchoredPosition_m4DD45DB1A97734A1F3A81E5F259638ECAF35962F(L_1, L_8, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_9, /*hidden argument*/NULL);
		bool L_11 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_10, _stringLiteral356A192B7913B04C54574D18C28D46E6395428AB, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00b6;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_12, /*hidden argument*/NULL);
		bool L_14 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_13, _stringLiteralDA4B9237BACCCDF19C0760CAB7AEC4A8359010B0, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00b6;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_15, /*hidden argument*/NULL);
		bool L_17 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_16, _stringLiteral77DE68DAECD823BABBB58EDB1C8E14D7106E83BB, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00b6;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_18 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_18, /*hidden argument*/NULL);
		bool L_20 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_19, _stringLiteralFA35E192121EABF3DABF9F5EA6ABDBCBC107AC3B, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b6;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_21 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_21, /*hidden argument*/NULL);
		bool L_23 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_22, _stringLiteralF1ABD670358E036C31296E66B3B66C382AC00812, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00b6;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_24 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		String_t* L_25 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_24, /*hidden argument*/NULL);
		bool L_26 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_25, _stringLiteral1574BDDB75C78A6FD2251D61E2993B5146201319, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00d6;
		}
	}

IL_00b6:
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_27 = Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_28), (1.2f), (1.2f), (1.2f), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_27, L_28, /*hidden argument*/NULL);
		return;
	}

IL_00d6:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		String_t* L_30 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_29, /*hidden argument*/NULL);
		bool L_31 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_30, _stringLiteral1B6453892473A467D07372D45EB05ABC2031647A, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0132;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		String_t* L_33 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_32, /*hidden argument*/NULL);
		bool L_34 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_33, _stringLiteralC1DFD96EEA8CC2B62785275BCA38AC261256E278, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_0132;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_35 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_35, /*hidden argument*/NULL);
		bool L_37 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_36, _stringLiteral902BA3CDA1883801594B6E1B452790CC53948FDA, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_0132;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_38 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		String_t* L_39 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_38, /*hidden argument*/NULL);
		bool L_40 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_39, _stringLiteralB1D5781111D84F7B3FE45A0852E59758CD7A87E5, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0152;
		}
	}

IL_0132:
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_41 = Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_42), (0.96f), (0.96f), (0.96f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_41, L_42, /*hidden argument*/NULL);
		return;
	}

IL_0152:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_43 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		String_t* L_44 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_43, /*hidden argument*/NULL);
		bool L_45 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_44, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_01ae;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_46 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		String_t* L_47 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_46, /*hidden argument*/NULL);
		bool L_48 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_47, _stringLiteral17BA0791499DB908433B80F37C5FBC89B870084B, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01ae;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_49 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		String_t* L_50 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_49, /*hidden argument*/NULL);
		bool L_51 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_50, _stringLiteral7B52009B64FD0A2A49E6D8A939753077792B0554, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_01ae;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_52 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		String_t* L_53 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_52, /*hidden argument*/NULL);
		bool L_54 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_53, _stringLiteralBD307A3EC329E10A2CFF8FB87480823DA114F8F4, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_01ce;
		}
	}

IL_01ae:
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_55 = Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_56;
		memset((&L_56), 0, sizeof(L_56));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_56), (1.08f), (1.08f), (1.08f), /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_55, L_56, /*hidden argument*/NULL);
		return;
	}

IL_01ce:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_57 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_58 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_57, /*hidden argument*/NULL);
		bool L_59 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_58, _stringLiteralFE5DBBCEA5CE7E2988B8C69BCFDFDE8904AABC1F, /*hidden argument*/NULL);
		if (L_59)
		{
			goto IL_01fc;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_60 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_60);
		String_t* L_61 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_60, /*hidden argument*/NULL);
		bool L_62 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_61, _stringLiteral0ADE7C2CF97F75D009975F4D720D1FA6C19F4897, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_021b;
		}
	}

IL_01fc:
	{
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_63 = Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_mEF939F54B6B56187EC11E16F51DCB12EB62C2103_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_64;
		memset((&L_64), 0, sizeof(L_64));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_64), (0.72f), (0.72f), (0.72f), /*hidden argument*/NULL);
		NullCheck(L_63);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_63, L_64, /*hidden argument*/NULL);
	}

IL_021b:
	{
		return;
	}
}
// System.Void DragDrop::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragDrop_OnEndDrag_m9FB4EF67A7C0316A93A73AD2DD0C6B8FB30E48FB (DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragDrop_OnEndDrag_m9FB4EF67A7C0316A93A73AD2DD0C6B8FB30E48FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_0 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_1 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_2 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_3 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_4 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_5 = NULL;
	{
		CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * L_0 = __this->get_canvasGroup_7();
		NullCheck(L_0);
		CanvasGroup_set_alpha_m7E3C4DCD13E6B1FD43C797EFF9698BACA1FBEC3D(L_0, (1.0f), /*hidden argument*/NULL);
		CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * L_1 = __this->get_canvasGroup_7();
		NullCheck(L_1);
		CanvasGroup_set_blocksRaycasts_m94D595956A88573219381E8F79A2B7CBEBCE7B10(L_1, (bool)1, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_2 = __this->get_slot_4();
		NullCheck(L_2);
		AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		bool L_3 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_slotCleanser_9();
		if (!L_3)
		{
			goto IL_013d;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_4, /*hidden argument*/NULL);
		__this->set_objectName_8(L_5);
		String_t* L_6 = __this->get_objectName_8();
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotCleaser_10(L_6);
		String_t* L_7 = __this->get_objectName_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = V_0;
		NullCheck(L_9);
		DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * L_10 = GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C(L_9, /*hidden argument*/GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C_RuntimeMethod_var);
		NullCheck(L_10);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = V_0;
		NullCheck(L_11);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_12 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_11, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_13), (-580.0f), (370.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_12, L_13, /*hidden argument*/NULL);
		String_t* L_14 = __this->get_objectName_8();
		bool L_15 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_14, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00de;
		}
	}
	{
		String_t* L_16 = __this->get_objectName_8();
		bool L_17 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_16, _stringLiteralC1DFD96EEA8CC2B62785275BCA38AC261256E278, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00de;
		}
	}
	{
		String_t* L_18 = __this->get_objectName_8();
		bool L_19 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_18, _stringLiteralB1D5781111D84F7B3FE45A0852E59758CD7A87E5, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00de;
		}
	}
	{
		String_t* L_20 = __this->get_objectName_8();
		bool L_21 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_20, _stringLiteral7B52009B64FD0A2A49E6D8A939753077792B0554, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00de;
		}
	}
	{
		String_t* L_22 = __this->get_objectName_8();
		bool L_23 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_22, _stringLiteralBD307A3EC329E10A2CFF8FB87480823DA114F8F4, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00ff;
		}
	}

IL_00de:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_24 = V_0;
		NullCheck(L_24);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_25 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_24, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26;
		memset((&L_26), 0, sizeof(L_26));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_26), (0.7f), (0.7f), (0.7f), /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_25, L_26, /*hidden argument*/NULL);
		goto IL_0135;
	}

IL_00ff:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_27 = V_0;
		NullCheck(L_27);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_28 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_27, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		NullCheck(L_28);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_28, /*hidden argument*/NULL);
		float L_30 = L_29.get_x_2();
		if ((!(((float)L_30) > ((float)(0.9f)))))
		{
			goto IL_0135;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_31 = V_0;
		NullCheck(L_31);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_32 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_31, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33;
		memset((&L_33), 0, sizeof(L_33));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_33), (0.9f), (0.9f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_32, L_33, /*hidden argument*/NULL);
	}

IL_0135:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotCleanser_9((bool)0);
		goto IL_0158;
	}

IL_013d:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_34 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_35 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_34, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = __this->get_initPos_10();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_35, L_37, /*hidden argument*/NULL);
	}

IL_0158:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		bool L_38 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_slotMoistur_11();
		if (!L_38)
		{
			goto IL_0278;
		}
	}
	{
		int32_t L_39 = __this->get_gameType_9();
		if ((!(((uint32_t)L_39) == ((uint32_t)1))))
		{
			goto IL_0278;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_40 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_40, /*hidden argument*/NULL);
		__this->set_objectName_8(L_41);
		String_t* L_42 = __this->get_objectName_8();
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotMoistur_12(L_42);
		String_t* L_43 = __this->get_objectName_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_44 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(L_43, /*hidden argument*/NULL);
		V_1 = L_44;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_45 = V_1;
		NullCheck(L_45);
		DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * L_46 = GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C(L_45, /*hidden argument*/GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C_RuntimeMethod_var);
		NullCheck(L_46);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_46, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_47 = V_1;
		NullCheck(L_47);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_48 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_47, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49;
		memset((&L_49), 0, sizeof(L_49));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_49), (-325.0f), (100.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_48, L_49, /*hidden argument*/NULL);
		String_t* L_50 = __this->get_objectName_8();
		bool L_51 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_50, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_021b;
		}
	}
	{
		String_t* L_52 = __this->get_objectName_8();
		bool L_53 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_52, _stringLiteralC1DFD96EEA8CC2B62785275BCA38AC261256E278, /*hidden argument*/NULL);
		if (L_53)
		{
			goto IL_021b;
		}
	}
	{
		String_t* L_54 = __this->get_objectName_8();
		bool L_55 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_54, _stringLiteralB1D5781111D84F7B3FE45A0852E59758CD7A87E5, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_021b;
		}
	}
	{
		String_t* L_56 = __this->get_objectName_8();
		bool L_57 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_56, _stringLiteral7B52009B64FD0A2A49E6D8A939753077792B0554, /*hidden argument*/NULL);
		if (L_57)
		{
			goto IL_021b;
		}
	}
	{
		String_t* L_58 = __this->get_objectName_8();
		bool L_59 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_58, _stringLiteralBD307A3EC329E10A2CFF8FB87480823DA114F8F4, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_023c;
		}
	}

IL_021b:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_60 = V_1;
		NullCheck(L_60);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_61 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_60, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_62;
		memset((&L_62), 0, sizeof(L_62));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_62), (0.7f), (0.7f), (0.7f), /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_61, L_62, /*hidden argument*/NULL);
		goto IL_0272;
	}

IL_023c:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_63 = V_1;
		NullCheck(L_63);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_64 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_63, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		NullCheck(L_64);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_65 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_64, /*hidden argument*/NULL);
		float L_66 = L_65.get_x_2();
		if ((!(((float)L_66) > ((float)(0.9f)))))
		{
			goto IL_0272;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_67 = V_1;
		NullCheck(L_67);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_68 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_67, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_69;
		memset((&L_69), 0, sizeof(L_69));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_69), (0.9f), (0.9f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_68);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_68, L_69, /*hidden argument*/NULL);
	}

IL_0272:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotMoistur_11((bool)0);
	}

IL_0278:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		bool L_70 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_slotMoistur_11();
		if (!L_70)
		{
			goto IL_0398;
		}
	}
	{
		int32_t L_71 = __this->get_gameType_9();
		if ((!(((uint32_t)L_71) == ((uint32_t)2))))
		{
			goto IL_0398;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_72 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_72);
		String_t* L_73 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_72, /*hidden argument*/NULL);
		__this->set_objectName_8(L_73);
		String_t* L_74 = __this->get_objectName_8();
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotMoistur_12(L_74);
		String_t* L_75 = __this->get_objectName_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_76 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(L_75, /*hidden argument*/NULL);
		V_2 = L_76;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_77 = V_2;
		NullCheck(L_77);
		DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * L_78 = GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C(L_77, /*hidden argument*/GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C_RuntimeMethod_var);
		NullCheck(L_78);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_78, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_79 = V_2;
		NullCheck(L_79);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_80 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_79, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_81;
		memset((&L_81), 0, sizeof(L_81));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_81), (-325.0f), (370.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_80);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_80, L_81, /*hidden argument*/NULL);
		String_t* L_82 = __this->get_objectName_8();
		bool L_83 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_82, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		if (L_83)
		{
			goto IL_033b;
		}
	}
	{
		String_t* L_84 = __this->get_objectName_8();
		bool L_85 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_84, _stringLiteralC1DFD96EEA8CC2B62785275BCA38AC261256E278, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_033b;
		}
	}
	{
		String_t* L_86 = __this->get_objectName_8();
		bool L_87 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_86, _stringLiteralB1D5781111D84F7B3FE45A0852E59758CD7A87E5, /*hidden argument*/NULL);
		if (L_87)
		{
			goto IL_033b;
		}
	}
	{
		String_t* L_88 = __this->get_objectName_8();
		bool L_89 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_88, _stringLiteral7B52009B64FD0A2A49E6D8A939753077792B0554, /*hidden argument*/NULL);
		if (L_89)
		{
			goto IL_033b;
		}
	}
	{
		String_t* L_90 = __this->get_objectName_8();
		bool L_91 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_90, _stringLiteralBD307A3EC329E10A2CFF8FB87480823DA114F8F4, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_035c;
		}
	}

IL_033b:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_92 = V_2;
		NullCheck(L_92);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_93 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_92, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_94;
		memset((&L_94), 0, sizeof(L_94));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_94), (0.7f), (0.7f), (0.7f), /*hidden argument*/NULL);
		NullCheck(L_93);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_93, L_94, /*hidden argument*/NULL);
		goto IL_0392;
	}

IL_035c:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_95 = V_2;
		NullCheck(L_95);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_96 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_95, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		NullCheck(L_96);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_97 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_96, /*hidden argument*/NULL);
		float L_98 = L_97.get_x_2();
		if ((!(((float)L_98) > ((float)(0.9f)))))
		{
			goto IL_0392;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_99 = V_2;
		NullCheck(L_99);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_100 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_99, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_101;
		memset((&L_101), 0, sizeof(L_101));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_101), (0.9f), (0.9f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_100);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_100, L_101, /*hidden argument*/NULL);
	}

IL_0392:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotMoistur_11((bool)0);
	}

IL_0398:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		bool L_102 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_slotSun_13();
		if (!L_102)
		{
			goto IL_04b8;
		}
	}
	{
		int32_t L_103 = __this->get_gameType_9();
		if ((!(((uint32_t)L_103) == ((uint32_t)1))))
		{
			goto IL_04b8;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_104 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_104);
		String_t* L_105 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_104, /*hidden argument*/NULL);
		__this->set_objectName_8(L_105);
		String_t* L_106 = __this->get_objectName_8();
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSun_14(L_106);
		String_t* L_107 = __this->get_objectName_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_108 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(L_107, /*hidden argument*/NULL);
		V_3 = L_108;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_109 = V_3;
		NullCheck(L_109);
		DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * L_110 = GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C(L_109, /*hidden argument*/GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C_RuntimeMethod_var);
		NullCheck(L_110);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_110, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_111 = V_3;
		NullCheck(L_111);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_112 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_111, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_113;
		memset((&L_113), 0, sizeof(L_113));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_113), (-580.0f), (-180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_112);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_112, L_113, /*hidden argument*/NULL);
		String_t* L_114 = __this->get_objectName_8();
		bool L_115 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_114, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		if (L_115)
		{
			goto IL_045b;
		}
	}
	{
		String_t* L_116 = __this->get_objectName_8();
		bool L_117 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_116, _stringLiteralC1DFD96EEA8CC2B62785275BCA38AC261256E278, /*hidden argument*/NULL);
		if (L_117)
		{
			goto IL_045b;
		}
	}
	{
		String_t* L_118 = __this->get_objectName_8();
		bool L_119 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_118, _stringLiteralB1D5781111D84F7B3FE45A0852E59758CD7A87E5, /*hidden argument*/NULL);
		if (L_119)
		{
			goto IL_045b;
		}
	}
	{
		String_t* L_120 = __this->get_objectName_8();
		bool L_121 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_120, _stringLiteral7B52009B64FD0A2A49E6D8A939753077792B0554, /*hidden argument*/NULL);
		if (L_121)
		{
			goto IL_045b;
		}
	}
	{
		String_t* L_122 = __this->get_objectName_8();
		bool L_123 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_122, _stringLiteralBD307A3EC329E10A2CFF8FB87480823DA114F8F4, /*hidden argument*/NULL);
		if (!L_123)
		{
			goto IL_047c;
		}
	}

IL_045b:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_124 = V_3;
		NullCheck(L_124);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_125 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_124, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_126;
		memset((&L_126), 0, sizeof(L_126));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_126), (0.7f), (0.7f), (0.7f), /*hidden argument*/NULL);
		NullCheck(L_125);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_125, L_126, /*hidden argument*/NULL);
		goto IL_04b2;
	}

IL_047c:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_127 = V_3;
		NullCheck(L_127);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_128 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_127, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		NullCheck(L_128);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_129 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_128, /*hidden argument*/NULL);
		float L_130 = L_129.get_x_2();
		if ((!(((float)L_130) > ((float)(0.9f)))))
		{
			goto IL_04b2;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_131 = V_3;
		NullCheck(L_131);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_132 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_131, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_133;
		memset((&L_133), 0, sizeof(L_133));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_133), (0.9f), (0.9f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_132);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_132, L_133, /*hidden argument*/NULL);
	}

IL_04b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSun_13((bool)0);
	}

IL_04b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		bool L_134 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_slotSun_13();
		if (!L_134)
		{
			goto IL_05de;
		}
	}
	{
		int32_t L_135 = __this->get_gameType_9();
		if ((!(((uint32_t)L_135) == ((uint32_t)2))))
		{
			goto IL_05de;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_136 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_136);
		String_t* L_137 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_136, /*hidden argument*/NULL);
		__this->set_objectName_8(L_137);
		String_t* L_138 = __this->get_objectName_8();
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSun_14(L_138);
		String_t* L_139 = __this->get_objectName_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_140 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(L_139, /*hidden argument*/NULL);
		V_4 = L_140;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_141 = V_4;
		NullCheck(L_141);
		DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * L_142 = GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C(L_141, /*hidden argument*/GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C_RuntimeMethod_var);
		NullCheck(L_142);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_142, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_143 = V_4;
		NullCheck(L_143);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_144 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_143, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_145;
		memset((&L_145), 0, sizeof(L_145));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_145), (-580.0f), (-180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_144);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_144, L_145, /*hidden argument*/NULL);
		String_t* L_146 = __this->get_objectName_8();
		bool L_147 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_146, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		if (L_147)
		{
			goto IL_057e;
		}
	}
	{
		String_t* L_148 = __this->get_objectName_8();
		bool L_149 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_148, _stringLiteralC1DFD96EEA8CC2B62785275BCA38AC261256E278, /*hidden argument*/NULL);
		if (L_149)
		{
			goto IL_057e;
		}
	}
	{
		String_t* L_150 = __this->get_objectName_8();
		bool L_151 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_150, _stringLiteralB1D5781111D84F7B3FE45A0852E59758CD7A87E5, /*hidden argument*/NULL);
		if (L_151)
		{
			goto IL_057e;
		}
	}
	{
		String_t* L_152 = __this->get_objectName_8();
		bool L_153 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_152, _stringLiteral7B52009B64FD0A2A49E6D8A939753077792B0554, /*hidden argument*/NULL);
		if (L_153)
		{
			goto IL_057e;
		}
	}
	{
		String_t* L_154 = __this->get_objectName_8();
		bool L_155 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_154, _stringLiteralBD307A3EC329E10A2CFF8FB87480823DA114F8F4, /*hidden argument*/NULL);
		if (!L_155)
		{
			goto IL_05a0;
		}
	}

IL_057e:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_156 = V_4;
		NullCheck(L_156);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_157 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_156, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_158;
		memset((&L_158), 0, sizeof(L_158));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_158), (0.7f), (0.7f), (0.7f), /*hidden argument*/NULL);
		NullCheck(L_157);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_157, L_158, /*hidden argument*/NULL);
		goto IL_05d8;
	}

IL_05a0:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_159 = V_4;
		NullCheck(L_159);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_160 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_159, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		NullCheck(L_160);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_161 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_160, /*hidden argument*/NULL);
		float L_162 = L_161.get_x_2();
		if ((!(((float)L_162) > ((float)(0.9f)))))
		{
			goto IL_05d8;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_163 = V_4;
		NullCheck(L_163);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_164 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_163, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_165;
		memset((&L_165), 0, sizeof(L_165));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_165), (0.9f), (0.9f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_164);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_164, L_165, /*hidden argument*/NULL);
	}

IL_05d8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSun_13((bool)0);
	}

IL_05de:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		bool L_166 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_slotSpot_15();
		if (!L_166)
		{
			goto IL_06f8;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_167 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_167);
		String_t* L_168 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_167, /*hidden argument*/NULL);
		__this->set_objectName_8(L_168);
		String_t* L_169 = __this->get_objectName_8();
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSpot_16(L_169);
		String_t* L_170 = __this->get_objectName_8();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_171 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(L_170, /*hidden argument*/NULL);
		V_5 = L_171;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_172 = V_5;
		NullCheck(L_172);
		DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * L_173 = GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C(L_172, /*hidden argument*/GameObject_GetComponent_TisDragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675_m12F015F7E1A5064DE341F11C1A207FAC39262E8C_RuntimeMethod_var);
		NullCheck(L_173);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_173, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_174 = V_5;
		NullCheck(L_174);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_175 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_174, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_176;
		memset((&L_176), 0, sizeof(L_176));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_176), (-580.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_175);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_175, L_176, /*hidden argument*/NULL);
		String_t* L_177 = __this->get_objectName_8();
		bool L_178 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_177, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		if (L_178)
		{
			goto IL_0698;
		}
	}
	{
		String_t* L_179 = __this->get_objectName_8();
		bool L_180 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_179, _stringLiteralC1DFD96EEA8CC2B62785275BCA38AC261256E278, /*hidden argument*/NULL);
		if (L_180)
		{
			goto IL_0698;
		}
	}
	{
		String_t* L_181 = __this->get_objectName_8();
		bool L_182 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_181, _stringLiteralB1D5781111D84F7B3FE45A0852E59758CD7A87E5, /*hidden argument*/NULL);
		if (L_182)
		{
			goto IL_0698;
		}
	}
	{
		String_t* L_183 = __this->get_objectName_8();
		bool L_184 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_183, _stringLiteral7B52009B64FD0A2A49E6D8A939753077792B0554, /*hidden argument*/NULL);
		if (L_184)
		{
			goto IL_0698;
		}
	}
	{
		String_t* L_185 = __this->get_objectName_8();
		bool L_186 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_185, _stringLiteralBD307A3EC329E10A2CFF8FB87480823DA114F8F4, /*hidden argument*/NULL);
		if (!L_186)
		{
			goto IL_06ba;
		}
	}

IL_0698:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_187 = V_5;
		NullCheck(L_187);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_188 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_187, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_189;
		memset((&L_189), 0, sizeof(L_189));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_189), (0.7f), (0.7f), (0.7f), /*hidden argument*/NULL);
		NullCheck(L_188);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_188, L_189, /*hidden argument*/NULL);
		goto IL_06f2;
	}

IL_06ba:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_190 = V_5;
		NullCheck(L_190);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_191 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_190, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		NullCheck(L_191);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_192 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_191, /*hidden argument*/NULL);
		float L_193 = L_192.get_x_2();
		if ((!(((float)L_193) > ((float)(0.9f)))))
		{
			goto IL_06f2;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_194 = V_5;
		NullCheck(L_194);
		RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * L_195 = GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C(L_194, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20_m2E5F02DDA13C176AF75B4E7C1DB801D89E053B2C_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_196;
		memset((&L_196), 0, sizeof(L_196));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_196), (0.9f), (0.9f), (0.9f), /*hidden argument*/NULL);
		NullCheck(L_195);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_195, L_196, /*hidden argument*/NULL);
	}

IL_06f2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSpot_15((bool)0);
	}

IL_06f8:
	{
		return;
	}
}
// System.Void DragDrop::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragDrop_OnPointerDown_m2F50A2B6B615D3FD77947C938020C5848F5E4F7B (DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void DragDrop::OnDrop(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragDrop_OnDrop_m8E8E01564DDDC00325DE3609F65E7C5309C63418 (DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void DragDrop::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragDrop__ctor_m1FB1D4C3129DF42BFBEB7BBF58667210707D6B91 (DragDrop_t88ED6B9D819BB32B1D7F55431A4CA287F63AF675 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InfoControler::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InfoControler_Start_m28D5FAC3FE3A3E780899D97B5E01AD68FE0EA513 (InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InfoControler_Start_m28D5FAC3FE3A3E780899D97B5E01AD68FE0EA513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001a;
	}

IL_0004:
	{
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_0 = __this->get_info_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_RuntimeMethod_var);
		NullCheck(L_2);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_2, (bool)0, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_001a:
	{
		int32_t L_4 = V_0;
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_5 = __this->get_info_4();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_mA61C8F11E4F47F828FC3E31E04C25BF06F23B2A4_inline(L_5, /*hidden argument*/List_1_get_Count_mA61C8F11E4F47F828FC3E31E04C25BF06F23B2A4_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void InfoControler::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InfoControler_Update_m46184B20647AAEC95831C3CBF755420D2E5CA8AC (InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InfoControler_Update_m46184B20647AAEC95831C3CBF755420D2E5CA8AC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_001b;
		}
	}
	{
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_1 = __this->get_info_4();
		NullCheck(L_1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_inline(L_1, 0, /*hidden argument*/List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_RuntimeMethod_var);
		NullCheck(L_2);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0036;
		}
	}
	{
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_4 = __this->get_info_4();
		NullCheck(L_4);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_inline(L_4, 1, /*hidden argument*/List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_RuntimeMethod_var);
		NullCheck(L_5);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_5, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_6 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_0051;
		}
	}
	{
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_7 = __this->get_info_4();
		NullCheck(L_7);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_inline(L_7, 2, /*hidden argument*/List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_RuntimeMethod_var);
		NullCheck(L_8);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_8, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0051:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_9 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_9) == ((uint32_t)4))))
		{
			goto IL_006c;
		}
	}
	{
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_10 = __this->get_info_4();
		NullCheck(L_10);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_inline(L_10, 3, /*hidden argument*/List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_RuntimeMethod_var);
		NullCheck(L_11);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_11, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_006c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_12 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_12) == ((uint32_t)5))))
		{
			goto IL_0087;
		}
	}
	{
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_13 = __this->get_info_4();
		NullCheck(L_13);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_inline(L_13, 4, /*hidden argument*/List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_RuntimeMethod_var);
		NullCheck(L_14);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_14, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_15 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_15) == ((uint32_t)6))))
		{
			goto IL_00a1;
		}
	}
	{
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_16 = __this->get_info_4();
		NullCheck(L_16);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_inline(L_16, 5, /*hidden argument*/List_1_get_Item_mF3CFF4FB71CEEDC038A8826D6AE1A226B7CF22A6_RuntimeMethod_var);
		NullCheck(L_17);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_17, (bool)1, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		return;
	}
}
// System.Void InfoControler::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InfoControler_LoadScene_m226DB5DC40488E2212FE823F075BE1E93294B806 (InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9 * __this, String_t* ___sceneName0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___sceneName0;
		RuntimeObject* L_1 = InfoControler_ChangeScene_m347F912B0F276E3AB268E3563D905F575317AAAB(__this, L_0, (0.5f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator InfoControler::ChangeScene(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InfoControler_ChangeScene_m347F912B0F276E3AB268E3563D905F575317AAAB (InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9 * __this, String_t* ___sceneName0, float ___delay1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InfoControler_ChangeScene_m347F912B0F276E3AB268E3563D905F575317AAAB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * L_0 = (U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F *)il2cpp_codegen_object_new(U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F_il2cpp_TypeInfo_var);
		U3CChangeSceneU3Ed__4__ctor_mF840D806063E826EA7BD5155C96F1E1FD036A3A1(L_0, 0, /*hidden argument*/NULL);
		U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * L_1 = L_0;
		String_t* L_2 = ___sceneName0;
		NullCheck(L_1);
		L_1->set_sceneName_3(L_2);
		U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * L_3 = L_1;
		float L_4 = ___delay1;
		NullCheck(L_3);
		L_3->set_delay_2(L_4);
		return L_3;
	}
}
// System.Void InfoControler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InfoControler__ctor_m88E6FCBF6F67C43ABB9792CDD02288F464AA7955 (InfoControler_t84623909D146CC18D9D41482BFBC0A4031986ED9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InfoControler__ctor_m88E6FCBF6F67C43ABB9792CDD02288F464AA7955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3D4152882C54B77C712688E910390D5C8E030463 * L_0 = (List_1_t3D4152882C54B77C712688E910390D5C8E030463 *)il2cpp_codegen_object_new(List_1_t3D4152882C54B77C712688E910390D5C8E030463_il2cpp_TypeInfo_var);
		List_1__ctor_mE0CF797BC1662A4FDFF8009E76AC0A5CD1BB1FCA(L_0, /*hidden argument*/List_1__ctor_mE0CF797BC1662A4FDFF8009E76AC0A5CD1BB1FCA_RuntimeMethod_var);
		__this->set_info_4(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InfoControler_<ChangeScene>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__4__ctor_mF840D806063E826EA7BD5155C96F1E1FD036A3A1 (U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void InfoControler_<ChangeScene>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__4_System_IDisposable_Dispose_m0D74414CE97F9B5CC5F5D34D79E165C17607DA0D (U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean InfoControler_<ChangeScene>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CChangeSceneU3Ed__4_MoveNext_m894A21E18C7F0F9FA0834E4C7D2F4FE6D56D32BA (U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeSceneU3Ed__4_MoveNext_m894A21E18C7F0F9FA0834E4C7D2F4FE6D56D32BA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = __this->get_delay_2();
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_4 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0031:
	{
		__this->set_U3CU3E1__state_0((-1));
		String_t* L_5 = __this->get_sceneName_3();
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(L_5, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object InfoControler_<ChangeScene>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeSceneU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A055E5F2B7D4973F1BA059A9FF059079BE24836 (U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void InfoControler_<ChangeScene>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_Reset_m3A7E4485FF2B48AD4A7628AE61F7DCAE8BDB3DAD (U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_Reset_m3A7E4485FF2B48AD4A7628AE61F7DCAE8BDB3DAD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_Reset_m3A7E4485FF2B48AD4A7628AE61F7DCAE8BDB3DAD_RuntimeMethod_var);
	}
}
// System.Object InfoControler_<ChangeScene>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_get_Current_m030974C279098215AE52223B441EA5A98AFCC7FD (U3CChangeSceneU3Ed__4_t378753213D3D4B3C6F231C966D28DC660C4B4B5F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainControler::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainControler_Awake_m45D730272AF7272742302BF5F2BFE97DEA39F27E (MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainControler_Awake_m45D730272AF7272742302BF5F2BFE97DEA39F27E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* V_0 = NULL;
	int32_t V_1 = 0;
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = GameObject_FindGameObjectsWithTag_mF49A195F19A598C5FD145FFE175ABE5B4885FAD9(_stringLiteral70236C6C744FDD07EB3459FC64A09CDC1D7DA788, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_1 = V_0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		V_1 = 1;
		goto IL_0021;
	}

IL_0015:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_1;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))))
		{
			goto IL_0015;
		}
	}

IL_0027:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = __this->get_soundBg_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainControler::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainControler_Start_m526DA369C83B1398554DD8B6FB5E7072D13D6B99 (MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainControler_Start_m526DA369C83B1398554DD8B6FB5E7072D13D6B99_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_stateGame_4(0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_skinType_5(0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_submit_6((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotCleanser_9((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotCleaser_10((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotMoistur_11((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotMoistur_12((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSun_13((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSun_14((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSpot_15((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSpot_16((String_t*)NULL);
		return;
	}
}
// System.Void MainControler::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381 (MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8 * __this, String_t* ___sceneName0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___sceneName0;
		RuntimeObject* L_1 = MainControler_ChangeScene_m16D7FC9E226ABC0D8D4BB94097067CB2ADDAC3C1(__this, L_0, (0.5f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MainControler::ChangeScene(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MainControler_ChangeScene_m16D7FC9E226ABC0D8D4BB94097067CB2ADDAC3C1 (MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8 * __this, String_t* ___sceneName0, float ___delay1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainControler_ChangeScene_m16D7FC9E226ABC0D8D4BB94097067CB2ADDAC3C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * L_0 = (U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 *)il2cpp_codegen_object_new(U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7_il2cpp_TypeInfo_var);
		U3CChangeSceneU3Ed__6__ctor_m0456867FD259B715C25732F93DB08B0EC6AF4E75(L_0, 0, /*hidden argument*/NULL);
		U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * L_1 = L_0;
		String_t* L_2 = ___sceneName0;
		NullCheck(L_1);
		L_1->set_sceneName_3(L_2);
		U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * L_3 = L_1;
		float L_4 = ___delay1;
		NullCheck(L_3);
		L_3->set_delay_2(L_4);
		return L_3;
	}
}
// System.Void MainControler::SelectGame(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainControler_SelectGame_mB742DCA1262E691EF81FA220864E4E5E92F96D39 (MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8 * __this, int32_t ___numGame0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainControler_SelectGame_mB742DCA1262E691EF81FA220864E4E5E92F96D39_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___numGame0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0034;
			}
			case 2:
			{
				goto IL_0047;
			}
			case 3:
			{
				goto IL_005a;
			}
			case 4:
			{
				goto IL_006d;
			}
			case 5:
			{
				goto IL_0080;
			}
		}
	}
	{
		return;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_skinType_5(1);
		String_t* L_1 = __this->get_gotoScene_4();
		MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381(__this, L_1, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_skinType_5(2);
		String_t* L_2 = __this->get_gotoScene_4();
		MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381(__this, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_skinType_5(3);
		String_t* L_3 = __this->get_gotoScene_4();
		MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381(__this, L_3, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_skinType_5(4);
		String_t* L_4 = __this->get_gotoScene_4();
		MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381(__this, L_4, /*hidden argument*/NULL);
		return;
	}

IL_006d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_skinType_5(5);
		String_t* L_5 = __this->get_gotoScene_4();
		MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381(__this, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_skinType_5(6);
		String_t* L_6 = __this->get_gotoScene_4();
		MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainControler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainControler__ctor_m0A76356F06D15E90EA6EDA20AF5122B71C1FFCEB (MainControler_t8A48F9BAF785D689E6B3688EAFC082AD3C607FF8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainControler_<ChangeScene>d__6::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__6__ctor_m0456867FD259B715C25732F93DB08B0EC6AF4E75 (U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MainControler_<ChangeScene>d__6::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__6_System_IDisposable_Dispose_mDD974A73357B008576CAB760534E5693A48D8E22 (U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MainControler_<ChangeScene>d__6::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CChangeSceneU3Ed__6_MoveNext_mA7244BBB01C847F0C8D322C9C620D61AE26636B7 (U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeSceneU3Ed__6_MoveNext_mA7244BBB01C847F0C8D322C9C620D61AE26636B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = __this->get_delay_2();
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_4 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0031:
	{
		__this->set_U3CU3E1__state_0((-1));
		String_t* L_5 = __this->get_sceneName_3();
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(L_5, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object MainControler_<ChangeScene>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1282FEE088E488BAE82BAF74747D5DCA1F7F0788 (U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MainControler_<ChangeScene>d__6::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_Reset_m2FC6AAF41A7ABEF8C1AA8FABEFCDA00DCCAFD0FB (U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_Reset_m2FC6AAF41A7ABEF8C1AA8FABEFCDA00DCCAFD0FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_Reset_m2FC6AAF41A7ABEF8C1AA8FABEFCDA00DCCAFD0FB_RuntimeMethod_var);
	}
}
// System.Object MainControler_<ChangeScene>d__6::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m8F21B598C7F6F1916F0A5ACBA856963BD67FAA8F (U3CChangeSceneU3Ed__6_t631ED9420D353F9E2562141B211FD1B65F4214F7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainGame::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainGame_Awake_mDF3416E8B1FFA2206B605A56DBC967CACCD69C64 (MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_Awake_mDF3416E8B1FFA2206B605A56DBC967CACCD69C64_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotCleanser_9((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotCleaser_10((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotMoistur_11((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotMoistur_12((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSun_13((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSun_14((String_t*)NULL);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSpot_15((bool)0);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_inSlotSpot_16((String_t*)NULL);
		int32_t L_0 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_2 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((((int32_t)L_3) == ((int32_t)4)))
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_4 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_4) == ((uint32_t)6))))
		{
			goto IL_01c9;
		}
	}

IL_005b:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_canvasType1_4();
		NullCheck(L_5);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_5, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = __this->get_canvasType2_5();
		NullCheck(L_6);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_6, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_00b9;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = __this->get_textTitle_8();
		NullCheck(L_8);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_9 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_8, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteralBF7FE6E4F6BE348F80B339B934AF0432580C1236);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteralBF7FE6E4F6BE348F80B339B934AF0432580C1236);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_10);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = __this->get_imgInfo_9();
		NullCheck(L_11);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_12 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_11, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_13 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteralF2DD977A37B96AB4E037E48C20702C784EE30817, /*hidden argument*/NULL);
		NullCheck(L_12);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_12, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_13, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_01c9;
	}

IL_00b9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_14 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_00ff;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = __this->get_textTitle_8();
		NullCheck(L_15);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_16 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_15, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteral766C862EA4543A76F2D575E115DF348C13B0ABE5);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral766C862EA4543A76F2D575E115DF348C13B0ABE5);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, L_17);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_18 = __this->get_imgInfo_9();
		NullCheck(L_18);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_19 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_18, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_20 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral020A7BD137D302A5D7AEBB60C1EA77B8A4503434, /*hidden argument*/NULL);
		NullCheck(L_19);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_19, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_20, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_01c9;
	}

IL_00ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_21 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_21) == ((uint32_t)3))))
		{
			goto IL_0145;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = __this->get_textTitle_8();
		NullCheck(L_22);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_23 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_22, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteral90CC9BE4C49D3DFE4B74878E334E10721C4141F3);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral90CC9BE4C49D3DFE4B74878E334E10721C4141F3);
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_23, L_24);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_25 = __this->get_imgInfo_9();
		NullCheck(L_25);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_26 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_25, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_27 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral34B70FB2E9A7D5F879F77661E102E21A1EA349D2, /*hidden argument*/NULL);
		NullCheck(L_26);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_26, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_27, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_01c9;
	}

IL_0145:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_28 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_28) == ((uint32_t)4))))
		{
			goto IL_0188;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = __this->get_textTitle_8();
		NullCheck(L_29);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_30 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_29, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteral5D055A20B62B7CAA1924E3AC313D52F853E8D1E2);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral5D055A20B62B7CAA1924E3AC313D52F853E8D1E2);
		NullCheck(L_30);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_30, L_31);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = __this->get_imgInfo_9();
		NullCheck(L_32);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_33 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_32, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_34 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral3558E7A15B68B02593F56F3DCE046E82FBC5F540, /*hidden argument*/NULL);
		NullCheck(L_33);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_33, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_34, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_01c9;
	}

IL_0188:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_35 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_35) == ((uint32_t)6))))
		{
			goto IL_01c9;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_36 = __this->get_textTitle_8();
		NullCheck(L_36);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_37 = GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A(L_36, /*hidden argument*/GameObject_GetComponent_TisText_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_m24A42DAE3900B867697FFD9DFB6E448D6978CD4A_RuntimeMethod_var);
		NullCheck(_stringLiteral58243454356D7842EC6DB44B46D1E7DD93F8F094);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral58243454356D7842EC6DB44B46D1E7DD93F8F094);
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, L_38);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_39 = __this->get_imgInfo_9();
		NullCheck(L_39);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_40 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_39, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_41 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral66D86268C2DB6BE8C1C0C888C42741C953228575, /*hidden argument*/NULL);
		NullCheck(L_40);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_40, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_41, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_01c9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		int32_t L_42 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_skinType_5();
		if ((!(((uint32_t)L_42) == ((uint32_t)5))))
		{
			goto IL_01e9;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_43 = __this->get_canvasType1_4();
		NullCheck(L_43);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_43, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_44 = __this->get_canvasType2_5();
		NullCheck(L_44);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_44, (bool)1, /*hidden argument*/NULL);
	}

IL_01e9:
	{
		return;
	}
}
// System.Void MainGame::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainGame_Update_m5806921D50C18B13698814FF871A5C2D47605895 (MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_Update_m5806921D50C18B13698814FF871A5C2D47605895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotCleaser_10();
		if (L_0)
		{
			goto IL_00a4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_1 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotMoistur_12();
		if (L_1)
		{
			goto IL_00a4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_2 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSun_14();
		if (L_2)
		{
			goto IL_00a4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		String_t* L_3 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_inSlotSpot_16();
		if (L_3)
		{
			goto IL_00a4;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_submitBtn_6();
		NullCheck(L_4);
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_5 = GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80(L_4, /*hidden argument*/GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80_RuntimeMethod_var);
		NullCheck(L_5);
		Selectable_set_interactable_mF0897CD627B603DE1F3714FFD8B121AB694E0B6B(L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = __this->get_submitBtn2_7();
		NullCheck(L_6);
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_7 = GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80(L_6, /*hidden argument*/GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80_RuntimeMethod_var);
		NullCheck(L_7);
		Selectable_set_interactable_mF0897CD627B603DE1F3714FFD8B121AB694E0B6B(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = __this->get_submitBtn_6();
		NullCheck(L_8);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_9 = GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75(L_8, /*hidden argument*/GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75_RuntimeMethod_var);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2((&L_10), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)180), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_11 = Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_11);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = __this->get_submitBtn2_7();
		NullCheck(L_12);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_13 = GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75_RuntimeMethod_var);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2((&L_14), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)180), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_15 = Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_15);
		return;
	}

IL_00a4:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = __this->get_submitBtn_6();
		NullCheck(L_16);
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_17 = GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80(L_16, /*hidden argument*/GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80_RuntimeMethod_var);
		NullCheck(L_17);
		Selectable_set_interactable_mF0897CD627B603DE1F3714FFD8B121AB694E0B6B(L_17, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_18 = __this->get_submitBtn2_7();
		NullCheck(L_18);
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_19 = GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80(L_18, /*hidden argument*/GameObject_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m04725F873F765AF3EA906137A60CD58809FE1C80_RuntimeMethod_var);
		NullCheck(L_19);
		Selectable_set_interactable_mF0897CD627B603DE1F3714FFD8B121AB694E0B6B(L_19, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = __this->get_submitBtn_6();
		NullCheck(L_20);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_21 = GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75(L_20, /*hidden argument*/GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75_RuntimeMethod_var);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2((&L_22), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_23 = Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker1< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_21, L_23);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_24 = __this->get_submitBtn2_7();
		NullCheck(L_24);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_25 = GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75(L_24, /*hidden argument*/GameObject_GetComponent_TisImage_t18FED07D8646917E1C563745518CF3DD57FF0B3E_m9008044B066CF5830B44DEB0C6CD94872D501A75_RuntimeMethod_var);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_26;
		memset((&L_26), 0, sizeof(L_26));
		Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2((&L_26), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_27 = Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		VirtActionInvoker1< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_25, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_submit_6((bool)1);
		return;
	}
}
// System.Void MainGame::restart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainGame_restart_m29651E434BC5E96E1AD0EC441C0602FF3A37B518 (MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_restart_m29651E434BC5E96E1AD0EC441C0602FF3A37B518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = MainGame_ChangeScene_m4D0D38C5953215317DD0431939F4F2724E0E9292(__this, _stringLiteralD5553574F33C21CC970F71B86BD213C5C8ECC562, (0.5f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::submit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainGame_submit_mE6C0F744EEF994D9BF3E0B92458C26A38165FD0B (MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_submit_mE6C0F744EEF994D9BF3E0B92458C26A38165FD0B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		bool L_0 = ((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->get_submit_6();
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_stateGame_4(1);
		RuntimeObject* L_1 = MainGame_ChangeScene_m4D0D38C5953215317DD0431939F4F2724E0E9292(__this, _stringLiteral807FBED80282E04DFDA8998F08C143FF2D73F591, (0.5f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_1, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Collections.IEnumerator MainGame::ChangeScene(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MainGame_ChangeScene_m4D0D38C5953215317DD0431939F4F2724E0E9292 (MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72 * __this, String_t* ___sceneName0, float ___delay1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_ChangeScene_m4D0D38C5953215317DD0431939F4F2724E0E9292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * L_0 = (U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 *)il2cpp_codegen_object_new(U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4_il2cpp_TypeInfo_var);
		U3CChangeSceneU3Ed__10__ctor_mBB94D50E14BDD3FA29E34EC6586438B6E618B3D3(L_0, 0, /*hidden argument*/NULL);
		U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * L_1 = L_0;
		String_t* L_2 = ___sceneName0;
		NullCheck(L_1);
		L_1->set_sceneName_3(L_2);
		U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * L_3 = L_1;
		float L_4 = ___delay1;
		NullCheck(L_3);
		L_3->set_delay_2(L_4);
		return L_3;
	}
}
// System.Void MainGame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainGame__ctor_m578BA203101ADFCFB8CF1703F302DE487EEC4B51 (MainGame_t1009691C67C9A5D67AF8890E9EC23F1CD183BF72 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainGame_<ChangeScene>d__10::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__10__ctor_mBB94D50E14BDD3FA29E34EC6586438B6E618B3D3 (U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MainGame_<ChangeScene>d__10::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__10_System_IDisposable_Dispose_m77889D8696235D4CFCF1AEE70FF1DF8A7FE726E0 (U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MainGame_<ChangeScene>d__10::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CChangeSceneU3Ed__10_MoveNext_mEA9C7EC5BD998EB9033700AFB592D5621AFF0D00 (U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeSceneU3Ed__10_MoveNext_mEA9C7EC5BD998EB9033700AFB592D5621AFF0D00_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = __this->get_delay_2();
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_4 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0031:
	{
		__this->set_U3CU3E1__state_0((-1));
		String_t* L_5 = __this->get_sceneName_3();
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(L_5, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object MainGame_<ChangeScene>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CE9CD06D1B96ABD2EB150AE5FAA4A0D0E99DDC5 (U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MainGame_<ChangeScene>d__10::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_Reset_m5B4297928B22FE91DCDE0FD042ECADE85D23E59F (U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_Reset_m5B4297928B22FE91DCDE0FD042ECADE85D23E59F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_Reset_m5B4297928B22FE91DCDE0FD042ECADE85D23E59F_RuntimeMethod_var);
	}
}
// System.Object MainGame_<ChangeScene>d__10::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_get_Current_m0CDAF43849772FA305E5C46FC15E8C88787629A4 (U3CChangeSceneU3Ed__10_t327DE4104833C9F79954EF5BA3FA83F5D15C27D4 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SlotCleanser::OnDrop(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlotCleanser_OnDrop_m11F2388DC4A6D511D9F523C633FBE519F6FB3379 (SlotCleanser_t935FCD20D87326A7812CB0B340762E1F797D4987 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlotCleanser_OnDrop_m11F2388DC4A6D511D9F523C633FBE519F6FB3379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_onDroped_4();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		__this->set_onDroped_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotCleanser_9((bool)1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_2 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_1, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_3 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral9E5C601A0DD0DE759E9F6A432BA8A08D7271C5B6, /*hidden argument*/NULL);
		NullCheck(L_2);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_2, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_3, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SlotCleanser::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlotCleanser__ctor_m0DD8EA00EA842093DB99609B22714AFA8A8D922F (SlotCleanser_t935FCD20D87326A7812CB0B340762E1F797D4987 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SlotMoistur::OnDrop(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlotMoistur_OnDrop_m955C487F74BFAB6A675831FC4400223A155E6070 (SlotMoistur_t6399F50D86CE9D2077A6730E3180A519FB5180C5 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlotMoistur_OnDrop_m955C487F74BFAB6A675831FC4400223A155E6070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_onDroped_4();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		__this->set_onDroped_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotMoistur_11((bool)1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_2 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_1, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_3 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral9E5C601A0DD0DE759E9F6A432BA8A08D7271C5B6, /*hidden argument*/NULL);
		NullCheck(L_2);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_2, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_3, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SlotMoistur::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlotMoistur__ctor_m7E5ACD116C27FF3C0A366A3A7C7683EB95046F9A (SlotMoistur_t6399F50D86CE9D2077A6730E3180A519FB5180C5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SlotSpotArea::OnDrop(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlotSpotArea_OnDrop_m7D7565151BC53C159635EDA55055FBB522DAA485 (SlotSpotArea_tA8A8FB3BFFBED826AA161E415B9C1070DBFE7833 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlotSpotArea_OnDrop_m7D7565151BC53C159635EDA55055FBB522DAA485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_onDroped_4();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		__this->set_onDroped_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSpot_15((bool)1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_2 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_1, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_3 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral9E5C601A0DD0DE759E9F6A432BA8A08D7271C5B6, /*hidden argument*/NULL);
		NullCheck(L_2);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_2, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_3, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SlotSpotArea::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlotSpotArea__ctor_m58B5E9F3ADCD4F132C90A0EB3C1621559F38436F (SlotSpotArea_tA8A8FB3BFFBED826AA161E415B9C1070DBFE7833 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SlotSun::OnDrop(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlotSun_OnDrop_m933329E9471F1DA381C7641B2683FF24F622E449 (SlotSun_t51356D9D4E1522336062D53A0D61E6AC3DC826D0 * __this, PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SlotSun_OnDrop_m933329E9471F1DA381C7641B2683FF24F622E449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_onDroped_4();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		__this->set_onDroped_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var);
		((Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_StaticFields*)il2cpp_codegen_static_fields_for(Confic_tAF77BE5DD76D47DF01A6AC1A35A42A1B0EE7B400_il2cpp_TypeInfo_var))->set_slotSun_13((bool)1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_2 = GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28(L_1, /*hidden argument*/GameObject_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m0093BA716F788BA6E4A13053E6348CB8BC999C28_RuntimeMethod_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_3 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral9E5C601A0DD0DE759E9F6A432BA8A08D7271C5B6, /*hidden argument*/NULL);
		NullCheck(L_2);
		RawImage_set_texture_m63BC52D3B64A3BFD0EC182034FDD51E9A46F99F9(L_2, ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)IsInstSealed((RuntimeObject*)L_3, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SlotSun::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlotSun__ctor_mAF1FE78E8BB112EB7C534A8A5D77BA2E2983135F (SlotSun_t51356D9D4E1522336062D53A0D61E6AC3DC826D0 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  PointerEventData_get_delta_mC5D62E985D40A7708316C6E07B699B96D9C8184E_inline (PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * __this, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_U3CdeltaU3Ek__BackingField_13();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
