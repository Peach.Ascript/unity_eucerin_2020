﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AfterGame::Start()
extern void AfterGame_Start_m7244FDB25E5B98EFA493A3E1B83DFBF86CD64522 ();
// 0x00000002 System.Void AfterGame::Answer()
extern void AfterGame_Answer_mF50AFDC50177BBC66D74B15CC8D3A90581931554 ();
// 0x00000003 System.Void AfterGame::AnsNull()
extern void AfterGame_AnsNull_m24DEEAE8E046C8162F4C79727B363504AEB978C6 ();
// 0x00000004 System.Void AfterGame::Done()
extern void AfterGame_Done_m1DC3F3C8B03270661318E575908F2B947AEE0786 ();
// 0x00000005 System.Collections.IEnumerator AfterGame::ChangeScene(System.String,System.Single)
extern void AfterGame_ChangeScene_m299E5279C8C9DE765D5D2400B1DA24086092DBC5 ();
// 0x00000006 System.Void AfterGame::.ctor()
extern void AfterGame__ctor_mF9663167CA6E12212C1A1E5AB30068D7094EC0F0 ();
// 0x00000007 System.Void Confic::.ctor()
extern void Confic__ctor_mA0EE2BF9E6BA062E0D35AF050AE8EEE0ED65B677 ();
// 0x00000008 System.Void Confic::.cctor()
extern void Confic__cctor_m27BD47F3EB837CDF1FF3A2844F6010336F066192 ();
// 0x00000009 System.Void DragDrop::Awake()
extern void DragDrop_Awake_m7B480CEE433ACC24AC26E9907300BD55344EA297 ();
// 0x0000000A System.Void DragDrop::Start()
extern void DragDrop_Start_m8B9764256A8BAE3C6CC26CDFA4196AF470AC57A8 ();
// 0x0000000B System.Void DragDrop::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnBeginDrag_mC2894502E70CDF5A5F8639C211BE4D73658F8A71 ();
// 0x0000000C System.Void DragDrop::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnDrag_m3F2E12B4B1977B305E53D9A5BFDECD05254833A2 ();
// 0x0000000D System.Void DragDrop::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnEndDrag_m9FB4EF67A7C0316A93A73AD2DD0C6B8FB30E48FB ();
// 0x0000000E System.Void DragDrop::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnPointerDown_m2F50A2B6B615D3FD77947C938020C5848F5E4F7B ();
// 0x0000000F System.Void DragDrop::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnDrop_m8E8E01564DDDC00325DE3609F65E7C5309C63418 ();
// 0x00000010 System.Void DragDrop::.ctor()
extern void DragDrop__ctor_m1FB1D4C3129DF42BFBEB7BBF58667210707D6B91 ();
// 0x00000011 System.Void InfoControler::Start()
extern void InfoControler_Start_m28D5FAC3FE3A3E780899D97B5E01AD68FE0EA513 ();
// 0x00000012 System.Void InfoControler::Update()
extern void InfoControler_Update_m46184B20647AAEC95831C3CBF755420D2E5CA8AC ();
// 0x00000013 System.Void InfoControler::LoadScene(System.String)
extern void InfoControler_LoadScene_m226DB5DC40488E2212FE823F075BE1E93294B806 ();
// 0x00000014 System.Collections.IEnumerator InfoControler::ChangeScene(System.String,System.Single)
extern void InfoControler_ChangeScene_m347F912B0F276E3AB268E3563D905F575317AAAB ();
// 0x00000015 System.Void InfoControler::.ctor()
extern void InfoControler__ctor_m88E6FCBF6F67C43ABB9792CDD02288F464AA7955 ();
// 0x00000016 System.Void MainControler::Awake()
extern void MainControler_Awake_m45D730272AF7272742302BF5F2BFE97DEA39F27E ();
// 0x00000017 System.Void MainControler::Start()
extern void MainControler_Start_m526DA369C83B1398554DD8B6FB5E7072D13D6B99 ();
// 0x00000018 System.Void MainControler::LoadScene(System.String)
extern void MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381 ();
// 0x00000019 System.Collections.IEnumerator MainControler::ChangeScene(System.String,System.Single)
extern void MainControler_ChangeScene_m16D7FC9E226ABC0D8D4BB94097067CB2ADDAC3C1 ();
// 0x0000001A System.Void MainControler::SelectGame(System.Int32)
extern void MainControler_SelectGame_mB742DCA1262E691EF81FA220864E4E5E92F96D39 ();
// 0x0000001B System.Void MainControler::.ctor()
extern void MainControler__ctor_m0A76356F06D15E90EA6EDA20AF5122B71C1FFCEB ();
// 0x0000001C System.Void MainGame::Awake()
extern void MainGame_Awake_mDF3416E8B1FFA2206B605A56DBC967CACCD69C64 ();
// 0x0000001D System.Void MainGame::Update()
extern void MainGame_Update_m5806921D50C18B13698814FF871A5C2D47605895 ();
// 0x0000001E System.Void MainGame::restart()
extern void MainGame_restart_m29651E434BC5E96E1AD0EC441C0602FF3A37B518 ();
// 0x0000001F System.Void MainGame::submit()
extern void MainGame_submit_mE6C0F744EEF994D9BF3E0B92458C26A38165FD0B ();
// 0x00000020 System.Collections.IEnumerator MainGame::ChangeScene(System.String,System.Single)
extern void MainGame_ChangeScene_m4D0D38C5953215317DD0431939F4F2724E0E9292 ();
// 0x00000021 System.Void MainGame::.ctor()
extern void MainGame__ctor_m578BA203101ADFCFB8CF1703F302DE487EEC4B51 ();
// 0x00000022 System.Void SlotCleanser::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void SlotCleanser_OnDrop_m11F2388DC4A6D511D9F523C633FBE519F6FB3379 ();
// 0x00000023 System.Void SlotCleanser::.ctor()
extern void SlotCleanser__ctor_m0DD8EA00EA842093DB99609B22714AFA8A8D922F ();
// 0x00000024 System.Void SlotMoistur::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void SlotMoistur_OnDrop_m955C487F74BFAB6A675831FC4400223A155E6070 ();
// 0x00000025 System.Void SlotMoistur::.ctor()
extern void SlotMoistur__ctor_m7E5ACD116C27FF3C0A366A3A7C7683EB95046F9A ();
// 0x00000026 System.Void SlotSpotArea::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void SlotSpotArea_OnDrop_m7D7565151BC53C159635EDA55055FBB522DAA485 ();
// 0x00000027 System.Void SlotSpotArea::.ctor()
extern void SlotSpotArea__ctor_m58B5E9F3ADCD4F132C90A0EB3C1621559F38436F ();
// 0x00000028 System.Void SlotSun::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void SlotSun_OnDrop_m933329E9471F1DA381C7641B2683FF24F622E449 ();
// 0x00000029 System.Void SlotSun::.ctor()
extern void SlotSun__ctor_mAF1FE78E8BB112EB7C534A8A5D77BA2E2983135F ();
// 0x0000002A System.Void AfterGame_<ChangeScene>d__39::.ctor(System.Int32)
extern void U3CChangeSceneU3Ed__39__ctor_mB6C44C3BA8154033CF3387AF0A689A5B730AB036 ();
// 0x0000002B System.Void AfterGame_<ChangeScene>d__39::System.IDisposable.Dispose()
extern void U3CChangeSceneU3Ed__39_System_IDisposable_Dispose_m2D912323752DEA50F6E88C0DA0B73F74F78B84FB ();
// 0x0000002C System.Boolean AfterGame_<ChangeScene>d__39::MoveNext()
extern void U3CChangeSceneU3Ed__39_MoveNext_mC64851BA814A99FA1F5D63B5A94BB39E3C65FFF0 ();
// 0x0000002D System.Object AfterGame_<ChangeScene>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeSceneU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF9C3477C108A663FC89AFFBA540CED2F4CBD441 ();
// 0x0000002E System.Void AfterGame_<ChangeScene>d__39::System.Collections.IEnumerator.Reset()
extern void U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_Reset_mA9C68B0346703290FD417406AF4AD20248281FD3 ();
// 0x0000002F System.Object AfterGame_<ChangeScene>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_get_Current_mABC25FC239B5A0C82674BF0E1C35A4E71E13A410 ();
// 0x00000030 System.Void InfoControler_<ChangeScene>d__4::.ctor(System.Int32)
extern void U3CChangeSceneU3Ed__4__ctor_mF840D806063E826EA7BD5155C96F1E1FD036A3A1 ();
// 0x00000031 System.Void InfoControler_<ChangeScene>d__4::System.IDisposable.Dispose()
extern void U3CChangeSceneU3Ed__4_System_IDisposable_Dispose_m0D74414CE97F9B5CC5F5D34D79E165C17607DA0D ();
// 0x00000032 System.Boolean InfoControler_<ChangeScene>d__4::MoveNext()
extern void U3CChangeSceneU3Ed__4_MoveNext_m894A21E18C7F0F9FA0834E4C7D2F4FE6D56D32BA ();
// 0x00000033 System.Object InfoControler_<ChangeScene>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeSceneU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A055E5F2B7D4973F1BA059A9FF059079BE24836 ();
// 0x00000034 System.Void InfoControler_<ChangeScene>d__4::System.Collections.IEnumerator.Reset()
extern void U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_Reset_m3A7E4485FF2B48AD4A7628AE61F7DCAE8BDB3DAD ();
// 0x00000035 System.Object InfoControler_<ChangeScene>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_get_Current_m030974C279098215AE52223B441EA5A98AFCC7FD ();
// 0x00000036 System.Void MainControler_<ChangeScene>d__6::.ctor(System.Int32)
extern void U3CChangeSceneU3Ed__6__ctor_m0456867FD259B715C25732F93DB08B0EC6AF4E75 ();
// 0x00000037 System.Void MainControler_<ChangeScene>d__6::System.IDisposable.Dispose()
extern void U3CChangeSceneU3Ed__6_System_IDisposable_Dispose_mDD974A73357B008576CAB760534E5693A48D8E22 ();
// 0x00000038 System.Boolean MainControler_<ChangeScene>d__6::MoveNext()
extern void U3CChangeSceneU3Ed__6_MoveNext_mA7244BBB01C847F0C8D322C9C620D61AE26636B7 ();
// 0x00000039 System.Object MainControler_<ChangeScene>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1282FEE088E488BAE82BAF74747D5DCA1F7F0788 ();
// 0x0000003A System.Void MainControler_<ChangeScene>d__6::System.Collections.IEnumerator.Reset()
extern void U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_Reset_m2FC6AAF41A7ABEF8C1AA8FABEFCDA00DCCAFD0FB ();
// 0x0000003B System.Object MainControler_<ChangeScene>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m8F21B598C7F6F1916F0A5ACBA856963BD67FAA8F ();
// 0x0000003C System.Void MainGame_<ChangeScene>d__10::.ctor(System.Int32)
extern void U3CChangeSceneU3Ed__10__ctor_mBB94D50E14BDD3FA29E34EC6586438B6E618B3D3 ();
// 0x0000003D System.Void MainGame_<ChangeScene>d__10::System.IDisposable.Dispose()
extern void U3CChangeSceneU3Ed__10_System_IDisposable_Dispose_m77889D8696235D4CFCF1AEE70FF1DF8A7FE726E0 ();
// 0x0000003E System.Boolean MainGame_<ChangeScene>d__10::MoveNext()
extern void U3CChangeSceneU3Ed__10_MoveNext_mEA9C7EC5BD998EB9033700AFB592D5621AFF0D00 ();
// 0x0000003F System.Object MainGame_<ChangeScene>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CE9CD06D1B96ABD2EB150AE5FAA4A0D0E99DDC5 ();
// 0x00000040 System.Void MainGame_<ChangeScene>d__10::System.Collections.IEnumerator.Reset()
extern void U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_Reset_m5B4297928B22FE91DCDE0FD042ECADE85D23E59F ();
// 0x00000041 System.Object MainGame_<ChangeScene>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_get_Current_m0CDAF43849772FA305E5C46FC15E8C88787629A4 ();
static Il2CppMethodPointer s_methodPointers[65] = 
{
	AfterGame_Start_m7244FDB25E5B98EFA493A3E1B83DFBF86CD64522,
	AfterGame_Answer_mF50AFDC50177BBC66D74B15CC8D3A90581931554,
	AfterGame_AnsNull_m24DEEAE8E046C8162F4C79727B363504AEB978C6,
	AfterGame_Done_m1DC3F3C8B03270661318E575908F2B947AEE0786,
	AfterGame_ChangeScene_m299E5279C8C9DE765D5D2400B1DA24086092DBC5,
	AfterGame__ctor_mF9663167CA6E12212C1A1E5AB30068D7094EC0F0,
	Confic__ctor_mA0EE2BF9E6BA062E0D35AF050AE8EEE0ED65B677,
	Confic__cctor_m27BD47F3EB837CDF1FF3A2844F6010336F066192,
	DragDrop_Awake_m7B480CEE433ACC24AC26E9907300BD55344EA297,
	DragDrop_Start_m8B9764256A8BAE3C6CC26CDFA4196AF470AC57A8,
	DragDrop_OnBeginDrag_mC2894502E70CDF5A5F8639C211BE4D73658F8A71,
	DragDrop_OnDrag_m3F2E12B4B1977B305E53D9A5BFDECD05254833A2,
	DragDrop_OnEndDrag_m9FB4EF67A7C0316A93A73AD2DD0C6B8FB30E48FB,
	DragDrop_OnPointerDown_m2F50A2B6B615D3FD77947C938020C5848F5E4F7B,
	DragDrop_OnDrop_m8E8E01564DDDC00325DE3609F65E7C5309C63418,
	DragDrop__ctor_m1FB1D4C3129DF42BFBEB7BBF58667210707D6B91,
	InfoControler_Start_m28D5FAC3FE3A3E780899D97B5E01AD68FE0EA513,
	InfoControler_Update_m46184B20647AAEC95831C3CBF755420D2E5CA8AC,
	InfoControler_LoadScene_m226DB5DC40488E2212FE823F075BE1E93294B806,
	InfoControler_ChangeScene_m347F912B0F276E3AB268E3563D905F575317AAAB,
	InfoControler__ctor_m88E6FCBF6F67C43ABB9792CDD02288F464AA7955,
	MainControler_Awake_m45D730272AF7272742302BF5F2BFE97DEA39F27E,
	MainControler_Start_m526DA369C83B1398554DD8B6FB5E7072D13D6B99,
	MainControler_LoadScene_mEB47F40E185EF95B7AB885EADD2869874F47A381,
	MainControler_ChangeScene_m16D7FC9E226ABC0D8D4BB94097067CB2ADDAC3C1,
	MainControler_SelectGame_mB742DCA1262E691EF81FA220864E4E5E92F96D39,
	MainControler__ctor_m0A76356F06D15E90EA6EDA20AF5122B71C1FFCEB,
	MainGame_Awake_mDF3416E8B1FFA2206B605A56DBC967CACCD69C64,
	MainGame_Update_m5806921D50C18B13698814FF871A5C2D47605895,
	MainGame_restart_m29651E434BC5E96E1AD0EC441C0602FF3A37B518,
	MainGame_submit_mE6C0F744EEF994D9BF3E0B92458C26A38165FD0B,
	MainGame_ChangeScene_m4D0D38C5953215317DD0431939F4F2724E0E9292,
	MainGame__ctor_m578BA203101ADFCFB8CF1703F302DE487EEC4B51,
	SlotCleanser_OnDrop_m11F2388DC4A6D511D9F523C633FBE519F6FB3379,
	SlotCleanser__ctor_m0DD8EA00EA842093DB99609B22714AFA8A8D922F,
	SlotMoistur_OnDrop_m955C487F74BFAB6A675831FC4400223A155E6070,
	SlotMoistur__ctor_m7E5ACD116C27FF3C0A366A3A7C7683EB95046F9A,
	SlotSpotArea_OnDrop_m7D7565151BC53C159635EDA55055FBB522DAA485,
	SlotSpotArea__ctor_m58B5E9F3ADCD4F132C90A0EB3C1621559F38436F,
	SlotSun_OnDrop_m933329E9471F1DA381C7641B2683FF24F622E449,
	SlotSun__ctor_mAF1FE78E8BB112EB7C534A8A5D77BA2E2983135F,
	U3CChangeSceneU3Ed__39__ctor_mB6C44C3BA8154033CF3387AF0A689A5B730AB036,
	U3CChangeSceneU3Ed__39_System_IDisposable_Dispose_m2D912323752DEA50F6E88C0DA0B73F74F78B84FB,
	U3CChangeSceneU3Ed__39_MoveNext_mC64851BA814A99FA1F5D63B5A94BB39E3C65FFF0,
	U3CChangeSceneU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF9C3477C108A663FC89AFFBA540CED2F4CBD441,
	U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_Reset_mA9C68B0346703290FD417406AF4AD20248281FD3,
	U3CChangeSceneU3Ed__39_System_Collections_IEnumerator_get_Current_mABC25FC239B5A0C82674BF0E1C35A4E71E13A410,
	U3CChangeSceneU3Ed__4__ctor_mF840D806063E826EA7BD5155C96F1E1FD036A3A1,
	U3CChangeSceneU3Ed__4_System_IDisposable_Dispose_m0D74414CE97F9B5CC5F5D34D79E165C17607DA0D,
	U3CChangeSceneU3Ed__4_MoveNext_m894A21E18C7F0F9FA0834E4C7D2F4FE6D56D32BA,
	U3CChangeSceneU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A055E5F2B7D4973F1BA059A9FF059079BE24836,
	U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_Reset_m3A7E4485FF2B48AD4A7628AE61F7DCAE8BDB3DAD,
	U3CChangeSceneU3Ed__4_System_Collections_IEnumerator_get_Current_m030974C279098215AE52223B441EA5A98AFCC7FD,
	U3CChangeSceneU3Ed__6__ctor_m0456867FD259B715C25732F93DB08B0EC6AF4E75,
	U3CChangeSceneU3Ed__6_System_IDisposable_Dispose_mDD974A73357B008576CAB760534E5693A48D8E22,
	U3CChangeSceneU3Ed__6_MoveNext_mA7244BBB01C847F0C8D322C9C620D61AE26636B7,
	U3CChangeSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1282FEE088E488BAE82BAF74747D5DCA1F7F0788,
	U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_Reset_m2FC6AAF41A7ABEF8C1AA8FABEFCDA00DCCAFD0FB,
	U3CChangeSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m8F21B598C7F6F1916F0A5ACBA856963BD67FAA8F,
	U3CChangeSceneU3Ed__10__ctor_mBB94D50E14BDD3FA29E34EC6586438B6E618B3D3,
	U3CChangeSceneU3Ed__10_System_IDisposable_Dispose_m77889D8696235D4CFCF1AEE70FF1DF8A7FE726E0,
	U3CChangeSceneU3Ed__10_MoveNext_mEA9C7EC5BD998EB9033700AFB592D5621AFF0D00,
	U3CChangeSceneU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CE9CD06D1B96ABD2EB150AE5FAA4A0D0E99DDC5,
	U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_Reset_m5B4297928B22FE91DCDE0FD042ECADE85D23E59F,
	U3CChangeSceneU3Ed__10_System_Collections_IEnumerator_get_Current_m0CDAF43849772FA305E5C46FC15E8C88787629A4,
};
static const int32_t s_InvokerIndices[65] = 
{
	23,
	23,
	23,
	23,
	1566,
	23,
	23,
	3,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	1566,
	23,
	23,
	23,
	26,
	1566,
	32,
	23,
	23,
	23,
	23,
	23,
	1566,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	65,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
